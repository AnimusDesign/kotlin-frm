package design.animus.kotlin.frm.sql.executors.jvm.vertx.mysql

import design.animus.kotlin.frm.sql.executors.common.IExecutorConfig

data class VertxMySQLConfig(
    override val databaseHost: String,
    override val database: String,
    override val databaseUser: String,
    override val databasePassword: String,
    override val databasePort: Int,
    val poolSize: Int = 25
) : IExecutorConfig