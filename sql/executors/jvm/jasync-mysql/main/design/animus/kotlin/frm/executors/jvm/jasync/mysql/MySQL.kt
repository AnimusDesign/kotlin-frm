package design.animus.kotlin.frm.executors.jvm.jasync.mysql

import com.github.jasync.sql.db.asSuspending
import com.github.jasync.sql.db.mysql.MySQLConnectionBuilder
import design.animus.kotlin.frm.executors.jvm.jasync.base.AJaSyncBase
import design.animus.kotlin.frm.sql.executors.common.exceptions.UnexpectedException

class JaSyncMySQLExecutor : AJaSyncBase<JaSyncMySQLConfig>() {
    // Connection
    @Throws(UnexpectedException::class)
    override suspend fun connect(config: JaSyncMySQLConfig): Boolean {
        val baseUrl = "jdbc:mysql://${config.databaseHost}:${config.databasePort}/${config.database}"
        try {
            logger.debug { "Establishing connection to $baseUrl" }
            val pool = MySQLConnectionBuilder.createConnectionPool(
                "$baseUrl?user=${config.databaseUser}&password=${config.databasePassword}"
            ).asSuspending
            connectionPool = pool
            logger.debug { "Connection pool -> $baseUrl created" }
            connected = true
            return true
        } catch (e: Exception) {
            logger.error(e) { "Failed to establish connection to database due to: ${e.message}" }
            throw UnexpectedException("Failed to establish connection to database due to: ${e.message}", e)
        }
    }

    override suspend fun disconnect() = try {
        logger.debug { "Disconnecting from database." }
        connectionPool.disconnect()
        logger.debug { "Disconnected from database." }
        true
    } catch (e: Exception) {
        logger.error(e) { "Failed to disconnect to database due to: ${e.message}" }
        false
    }
    // End Connection
}