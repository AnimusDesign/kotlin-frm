import design.animus.kotlin.frm.Versions.Dependencies

plugins {
    kotlin("jvm")
    id("maven-publish")
}

buildscript {
    apply("$rootDir/versions.gradle.kts")
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

kotlin {
    target {
        mavenPublication {
            artifactId = "executor-jasync-base"
        }
    }
    sourceSets {
        main {
            kotlin.setSrcDirs(
                    mutableListOf("main", "generated")
            )
            resources.srcDir("main/resources")
            dependencies {
                implementation(project(":sql:query:query-common:"))
                implementation("io.github.microutils:kotlin-logging-jvm:${Dependencies.kotlinLogging}")
                implementation(kotlin("stdlib-jdk8"))
                implementation("design.animus.kotlin.mp:datatypes:${Dependencies.MPDataTypes}")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Dependencies.coroutine}")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-jdk8:${Dependencies.coroutine}")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime:${Dependencies.serialization}")
                implementation("org.jetbrains.kotlin:kotlin-reflect:${Dependencies.kotlin}")
                implementation("org.postgresql:postgresql:${Dependencies.postgreSQL}")
                implementation("ch.qos.logback:logback-core:${Dependencies.logback}")
                implementation("ch.qos.logback:logback-classic:${Dependencies.logback}")
                implementation("com.github.jasync-sql:jasync-common:${Dependencies.jasync}")
                implementation("com.github.jasync-sql:jasync-postgresql:${Dependencies.jasync}")
                implementation("com.github.jasync-sql:jasync-mysql:${Dependencies.jasync}")
            }
        }
        test {
            kotlin.srcDir("test")
            resources.srcDir("test/resources")
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
                implementation(kotlin("test-junit"))
                implementation(kotlin("test"))
                implementation(project(":sql:query:query-common:"))
            }
        }
    }
}