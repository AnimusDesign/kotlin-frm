package design.animus.kotlin.frm.sql.executors.jvm.jasync

import design.animus.kotlin.frm.sql.executors.common.IExecutorConfig

data class JaSyncPostgresConfig(
    override val databaseHost: String,
    override val database: String,
    override val databaseUser: String,
    override val databasePassword: String,
    override val databasePort: Int,
    val schema: String
) : IExecutorConfig