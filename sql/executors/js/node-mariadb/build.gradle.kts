plugins {
    kotlin("js")
}

kotlin {
    target {
        useCommonJs()
        nodejs()
    }

    sourceSets {
        main {
            kotlin.srcDir("main")
            resources.srcDir("main/resources")
            dependencies {
                implementation(project(":sql:query:query-common:"))
                implementation("io.github.microutils:kotlin-logging-js:1.7.6")
                implementation(kotlin("stdlib-js"))
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.2")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-js:1.3.2")
//                implementation(npm("mariadb", "2.1.3"))
            }
        }
        test {
            kotlin.srcDir("test")
            resources.srcDir("test/resources")
            dependencies {
                implementation(kotlin("stdlib-js"))
                implementation(kotlin("test-js"))
                implementation(kotlin("test"))
            }
        }
    }

}

repositories {
    mavenLocal()
}
