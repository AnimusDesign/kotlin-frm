package design.animus.kotlin.frm.sql.schema.postgresql

import design.animus.kotlin.frm.sql.query.common.Common
import design.animus.kotlin.frm.sql.query.common.JVM
import design.animus.kotlin.frm.sql.query.common.JavaScript
import design.animus.kotlin.frm.sql.query.common.Platform
import design.animus.kotlin.frm.sql.query.postgres.query.PostgresTypeProvider
import design.animus.kotlin.frm.sql.schema.common.ISChemaGeneratorConfig
import design.animus.kotlin.frm.sql.schema.common.generator.generateSQLTableCode
import design.animus.kotlin.frm.sql.schema.postgresql.generator.DatabaseGenerator
import design.animus.kotlin.frm.sql.schema.postgresql.generator.generateEnumCode
import design.animus.kotlin.frm.sql.schema.postgresql.reflector.getCustomTypes
import design.animus.kotlin.frm.sql.schema.postgresql.reflector.normalizeData
import design.animus.kotlin.frm.sql.schema.postgresql.reflector.reflectSchema
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import org.gradle.api.Plugin
import org.gradle.api.Project

val logger = KotlinLogging.logger { }


open class PostgresSQLConfiguration : ISChemaGeneratorConfig {
    override var dataBases: List<String> = listOf()
    override var dataBaseHost: String = "localhost"
    override var dataBaseUser: String = "postgres"
    override var dataBasePassword: String = "postgres"
    override var dataBasePort: Int = 5432
    override var namespace: String = "design.animus.datasource.sql.generated"
    override var outputDir: String = "./"
    override var logLevel = "DEBUG"
    var dataBaseSchema: String = "public"
    override var platforms: Set<Platform> = setOf(Common, JavaScript, JVM)
    override var excludeTables = setOf<String>()
}

class PostgresSQLSchemaGenerator : Plugin<Project> {

    override fun apply(project: Project) {
        val extension = project.extensions.create<PostgresSQLConfiguration>(
            "PostgresGeneratorConfig",
            PostgresSQLConfiguration::class.java
        )
        project.task("checkConnection")
            .doLast { _ ->
                extension.dataBases.forEach { database ->
                    val connection = getPostgresConnection(database, extension)
                    connection.disconnect()
                }
            }
        project.task("buildSchema")
            .doLast { _ ->
                logger.info { "Running build schema" }
                extension.dataBases.forEach { db ->
                    runBlocking {
                        val conn = getPostgresConnection(db, extension)
                        val rawSchema = reflectSchema(conn, extension, db).filter {
                            !extension.excludeTables.contains(it.table)
                        }
                        val tables = normalizeData(rawSchema)
                        val databaseClass = DatabaseGenerator.generateDatabaseCode(db, extension)
                        val customTypes = getCustomTypes(conn, extension, db).groupBy { it.enumtype }
                            .map { (name, values) ->
                                generateEnumCode(name, values, extension)
                            }
                        tables.forEach { table ->
                            generateSQLTableCode(databaseClass, table, customTypes, PostgresTypeProvider, extension)
                        }
                        conn.disconnect()
                    }

                }
            }
    }
}
