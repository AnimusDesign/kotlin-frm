package design.animus.kotlin.frm.sql.schema.postgresql.utils

enum class SQLTypes(val friendly: String) {
    ARRAY("ARRAY"),
    Boolean("boolean"),
    SmallInt("smallint"),
    Integer("integer"),
    BigInt("bigint"),
    Numeric("numeric"),
    Real("real"),
    Double("double"),
    Text("text"),
    VarChar("varchar"),
    BPChar("bpchar"),
    TimeStamp("timestamp"),
    TimeStampWithTimeZone("timestamp_with_timezone"),
    Date("date"),
    Time("time"),
    UUID("uuid")
}

internal fun getSQLTypeByName(name: String) =
    SQLTypes.values().firstOrNull { it.friendly.toLowerCase() == name.toLowerCase() }
