package design.animus.kotlin.frm.sql.schema.postgresql.generator

import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.TypeSpec
import design.animus.kotlin.frm.sql.query.common.CustomType
import design.animus.kotlin.frm.sql.query.common.query.ISQLEnum
import design.animus.kotlin.frm.sql.schema.common.CodeWriter
import design.animus.kotlin.frm.sql.schema.common.renameClass
import design.animus.kotlin.frm.sql.schema.postgresql.PostgresSQLConfiguration
import design.animus.kotlin.frm.sql.schema.postgresql.reflector.EnumSchema


internal suspend fun buildEnumType(typeName: String, values: List<EnumSchema>): TypeSpec {
    val enumSpec = TypeSpec.enumBuilder(typeName)
        .addSuperinterface(
            ClassName(
                ISQLEnum::class.qualifiedName!!.replace(".ISQLEnum", ""),
                ISQLEnum::class.simpleName!!
            )
        )
        .primaryConstructor(
            FunSpec.constructorBuilder()
                .addParameter("friendly", String::class)
                .build()
        )
    values.forEach {
        val case = when {
            it.enumlabel.contains("-") -> it.enumlabel.replace("-", "").toUpperCase()
            it.enumlabel.contains("_") -> it.enumlabel.replace("_", "").toUpperCase()
            it.enumlabel.contains(" ") -> it.enumlabel.replace(" ", "").toUpperCase()
            else -> it.enumlabel.toUpperCase()
        }
        enumSpec.addEnumConstant(
            case,
            TypeSpec.anonymousClassBuilder()
                .addSuperclassConstructorParameter("%S", it.enumlabel.toLowerCase())
                .build()
        )
    }
    return enumSpec.build()
}

internal suspend fun generateEnumCode(
    typeName: String, values: List<EnumSchema>,
    config: PostgresSQLConfiguration
): CustomType {
    val writer = CodeWriter<PostgresSQLConfiguration>()
    val enumClassName = renameClass(typeName)
    val packageName = "${config.namespace}.database.types.enum"
    val type = buildEnumType(enumClassName, values)
    writer.platformWriter(packageName, enumClassName, config, type)
    return CustomType(
        sqlType = typeName,
        packageName = packageName,
        className = enumClassName
    )
}
