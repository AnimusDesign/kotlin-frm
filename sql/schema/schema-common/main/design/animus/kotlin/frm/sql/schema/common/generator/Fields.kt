package design.animus.kotlin.frm.sql.schema.common.generator

import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import design.animus.kotlin.frm.sql.query.common.*
import design.animus.kotlin.frm.sql.query.common.query.Field
import design.animus.kotlin.frm.sql.query.common.query.PrimaryKey
import design.animus.kotlin.frm.sql.schema.common.GeneratedField
import design.animus.kotlin.frm.sql.schema.common.ISChemaGeneratorConfig
import design.animus.kotlin.frm.sql.schema.common.records.Column
import design.animus.kotlin.frm.sql.schema.common.records.TableSchema
import design.animus.kotlin.frm.sql.schema.common.renameClass
import design.animus.kotlin.frm.sql.schema.common.renameProperty

class SQLFieldGenerator<C : ISChemaGeneratorConfig>(
    private val config: C,
    private val packageName: String,
    private val typeProvider: SQLTypeProvider,
    private val databaseClass: ClassName,
    private val recordClass: ClassName,
    private val schema: TableSchema,
    private val tableClassName: String,
    private val customTypes: CustomTypes
) {
    private val customTableInterface = ClassName(packageName, tableClassName)

    // Begin Helpers
    private fun buildFieldTypeHelper(
        col: Column,
        colType: ClassName
    ): ParameterizedTypeName {
        return if (!col.primaryKey) {
            ClassName(
                Field::class.qualifiedName!!.replace(".Field", ""),
                Field::class.simpleName!!
            )
                .parameterizedBy(
                    colType,
                    databaseClass,
                    recordClass,
                    customTableInterface
                )
        } else {
            ClassName(
                PrimaryKey::class.qualifiedName!!.replace(".PrimaryKey", ""),
                PrimaryKey::class.simpleName!!
            )
                .parameterizedBy(
                    colType,
                    databaseClass,
                    recordClass,
                    customTableInterface
                )
        }
    }

    private suspend fun buildPlatformField(
        multiPlatform: Boolean,
        platform: Platform
    ): List<GeneratedField> {
        val cols = schema.columns
        val customTableInterface = ClassName(packageName, tableClassName)
        return cols.map { col ->
            val typePlaceHolder = getRawType(col.type, platform, typeProvider, customTypes)
            val colType = ClassName(typePlaceHolder.packageName, typePlaceHolder.className)
            val colProp = renameProperty(col.name)
            val fieldClass = buildFieldTypeHelper(col, colType)
            val type = TypeSpec.objectBuilder(
                "${tableClassName}${renameClass(
                    col.name
                )}"
            )
                .superclass(fieldClass)
                .addSuperclassConstructorParameter("%S", col.name)
                .addSuperclassConstructorParameter("%S", col.type)
                .addSuperclassConstructorParameter("%S", schema.database)
                .addSuperclassConstructorParameter("%S", schema.tableName)
            if (multiPlatform) type.addModifiers(KModifier.ACTUAL)
            GeneratedField(col.name, colProp, fieldClass, type.build(), col.primaryKey, colType)
        }
    }
    // End Helpers

    suspend fun buildCommonFields(cols: List<Column>): List<GeneratedField> {
        return cols.map { col ->
            val colProp = renameProperty(col.name)
            val type = TypeSpec.objectBuilder(
                "${tableClassName}${renameClass(
                    col.name
                )}"
            )
                .addModifiers(KModifier.EXPECT)
            GeneratedField(
                col.name, colProp,
                buildFieldTypeHelper(col, String::class.asTypeName()), type.build(),
                col.primaryKey, String::class.asTypeName()
            )
        }
    }

    suspend fun buildJVMFields(multiPlatform: Boolean): List<GeneratedField> =
        buildPlatformField(multiPlatform, JVM)

    suspend fun buildJSFields(multiPlatform: Boolean): List<GeneratedField> =
        buildPlatformField(multiPlatform, JavaScript)

    suspend fun buildNativeFields(multiPlatform: Boolean): List<GeneratedField> =
        buildPlatformField(multiPlatform, Native)
}