package design.animus.kotlin.frm.sql.schema.common

fun renameProperty(prop: String): String {
    val phrases = prop.split("_")
    val rsp = mutableListOf<String>()
    rsp.add(phrases.first().decapitalize())
    rsp.addAll(
        phrases.subList(1, phrases.size).map {
            it.capitalize()
        }
    )
    return rsp.joinToString("")
}