package design.animus.kotlin.frm.sql.schema.common.generator

import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import design.animus.kotlin.frm.sql.query.common.*
import design.animus.kotlin.frm.sql.query.common.query.LateField
import design.animus.kotlin.frm.sql.schema.common.GeneratedField
import design.animus.kotlin.frm.sql.schema.common.ISChemaGeneratorConfig
import design.animus.kotlin.frm.sql.schema.common.records.Column
import design.animus.kotlin.frm.sql.schema.common.records.TableSchema
import design.animus.kotlin.frm.sql.schema.common.renameClass
import design.animus.kotlin.frm.sql.schema.common.renameProperty


class SQLLateFieldGenerator<C : ISChemaGeneratorConfig>(
    private val config: C,
    private val packageName: String,
    private val typeProvider: SQLTypeProvider,
    private val databaseClass: ClassName,
    private val recordClass: ClassName,
    private val schema: TableSchema,
    private val tableClassName: String,
    private val customTypes: CustomTypes
) {
    private val customTableInterface = ClassName(packageName, tableClassName)

    // Begin Helpers
    private fun buildFieldTypeHelper(colType: ClassName): ParameterizedTypeName =
        ClassName(
            LateField::class.qualifiedName!!.replace(".LateField", ""),
            LateField::class.simpleName!!
        )
            .parameterizedBy(
                colType,
                databaseClass,
                recordClass,
                customTableInterface
            )


    private suspend fun buildPlatformField(
        multiPlatform: Boolean,
        platform: Platform
    ): List<GeneratedField> {
        val cols = schema.columns
        val customTableInterface = ClassName(packageName, tableClassName)
        return cols.map { col ->
            val typePlaceHolder = getRawType(col.type, platform, typeProvider, customTypes)
            val colType = ClassName(typePlaceHolder.packageName, typePlaceHolder.className)
            val colProp = renameProperty(col.name)
            val fieldClass = buildFieldTypeHelper(colType)
            val type = TypeSpec.objectBuilder(
                "${tableClassName}Lazy${renameClass(col.name)}"
            )
                .superclass(fieldClass)
                .addSuperclassConstructorParameter("%S", colType.simpleName)
                .addSuperclassConstructorParameter("%S", col.name)
                .addSuperclassConstructorParameter("%S", col.type)
                .addSuperclassConstructorParameter("%S", schema.database)
                .addSuperclassConstructorParameter("%S", schema.tableName)
            if (multiPlatform) type.addModifiers(KModifier.ACTUAL)
            GeneratedField(col.name, "lazy${colProp.capitalize()}", fieldClass, type.build(), col.primaryKey, colType)
        }
    }
    // End Helpers

    suspend fun buildCommonLateFields(cols: List<Column>): List<GeneratedField> {
        return cols.map { col ->
            val colProp = renameProperty(col.name)
            val type = TypeSpec.objectBuilder(
                "${tableClassName}${renameClass(
                    col.name
                )}"
            )
                .addModifiers(KModifier.EXPECT)
            GeneratedField(
                col.name,
                "lazy${colProp}",
                buildFieldTypeHelper(String::class.asTypeName()),
                type.build(),
                col.primaryKey,
                String::class.asTypeName()
            )
        }
    }

    suspend fun buildJVMLateFields(multiPlatform: Boolean): List<GeneratedField> =
        buildPlatformField(multiPlatform, JVM)

    suspend fun buildJSLateFields(multiPlatform: Boolean): List<GeneratedField> =
        buildPlatformField(multiPlatform, JavaScript)

    suspend fun buildNativeLateFields(multiPlatform: Boolean): List<GeneratedField> =
        buildPlatformField(multiPlatform, Native)
}