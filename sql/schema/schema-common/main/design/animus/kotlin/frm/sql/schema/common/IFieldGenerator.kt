package design.animus.kotlin.frm.sql.schema.common

import com.squareup.kotlinpoet.ClassName

interface IFieldGenerator<C : ISChemaGeneratorConfig, S : IDBSchema, K : ISchemaKeyReflector> {
    suspend fun buildCommonFields(
        packageName: String,
        databaseClass: ClassName,
        tableClassName: String,
        recordClassName: String,
        recordClass: ClassName,
        cols: List<S>,
        keys: List<K>,
        config: C,
        multiPlatform: Boolean
    ): List<GeneratedField>


    suspend fun buildJVMFields(
        packageName: String,
        databaseClass: ClassName,
        tableClassName: String,
        recordClassName: String,
        recordClass: ClassName,
        cols: List<S>,
        keys: List<K>,
        config: C,
        multiPlatform: Boolean
    ): List<GeneratedField>

    suspend fun buildJSFields(
        packageName: String,
        databaseClass: ClassName,
        tableClassName: String,
        recordClassName: String,
        recordClass: ClassName,
        cols: List<S>,
        keys: List<K>,
        config: C,
        multiPlatform: Boolean
    ): List<GeneratedField>
}

interface ILateFieldGenerator<C : ISChemaGeneratorConfig, S : IDBSchema, K : ISchemaKeyReflector> {
    suspend fun buildCommonLateFields(
        packageName: String,
        databaseClass: ClassName,
        tableClassName: String,
        recordClassName: String,
        recordClass: ClassName,
        cols: List<S>,
        keys: List<K>,
        config: C,
        multiPlatform: Boolean
    ): List<GeneratedField>


    suspend fun buildJVMLateFields(
        packageName: String,
        databaseClass: ClassName,
        tableClassName: String,
        recordClassName: String,
        recordClass: ClassName,
        cols: List<S>,
        keys: List<K>,
        config: C,
        multiPlatform: Boolean
    ): List<GeneratedField>

    suspend fun buildJSLateFields(
        packageName: String,
        databaseClass: ClassName,
        tableClassName: String,
        recordClassName: String,
        recordClass: ClassName,
        cols: List<S>,
        keys: List<K>,
        config: C,
        multiPlatform: Boolean
    ): List<GeneratedField>
}