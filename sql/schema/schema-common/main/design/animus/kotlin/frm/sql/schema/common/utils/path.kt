package design.animus.kotlin.frm.sql.schema.common.utils

import design.animus.kotlin.frm.sql.schema.common.ISChemaGeneratorConfig
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

suspend fun getCommonMainPath(config: ISChemaGeneratorConfig): Path {
    val target = Paths.get(config.outputDir, "src", "commonMain", "kotlin")
    if (!target.toFile().exists()) Files.createDirectories(target)
    return target
}

suspend fun getJVMMainPath(config: ISChemaGeneratorConfig): Path =
    Paths.get(config.outputDir, "src", "jvmMain", "kotlin")

suspend fun getJSMainPath(config: ISChemaGeneratorConfig): Path = Paths.get(config.outputDir, "src", "jsMain", "kotlin")

suspend fun getSinglePlatform(config: ISChemaGeneratorConfig): Path = Paths.get(config.outputDir)

suspend fun getNativeMainPath(config: ISChemaGeneratorConfig): Path =
    Paths.get(config.outputDir, "src", "linuxX64Main", "kotlin")
