
###### Create Sample DB Container

```bash
docker run -d --name=Example -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=example -e POSTGRES_USER=postgres  -p 5432:5432 -v example_db:/var/lib/postgresql/data postgres
```

###### Initialize DB

```bash
gradle flywayMigrate -i
```