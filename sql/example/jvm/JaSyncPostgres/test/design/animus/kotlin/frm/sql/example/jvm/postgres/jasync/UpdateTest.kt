package design.animus.kotlin.frm.sql.example.jvm.postgres.jasync

import design.animus.kotlin.frm.sql.example.jvm.postgres.generated.database.postgres.tables.UserProfile
import design.animus.kotlin.frm.sql.example.jvm.postgres.generated.database.postgres.tables.Users
import design.animus.kotlin.frm.sql.executors.common.exceptions.EmptyResultSet
import design.animus.kotlin.frm.sql.executors.jvm.jasync.JaSyncPostgresExecutor
import design.animus.kotlin.frm.sql.query.postgres.frm.lazySelect
import design.animus.kotlin.frm.sql.query.postgres.frm.lazyUpdate
import kotlinx.coroutines.runBlocking
import org.junit.Test
import kotlin.test.AfterTest
import kotlin.test.BeforeTest
import kotlin.test.assertTrue

class UpdateTest {
    lateinit var executor: JaSyncPostgresExecutor

    @BeforeTest
    fun initialize() = runBlocking {
        executor = JaSyncPostgresExecutor()
        executor.connect(FRMTestConfig)
        Unit
    }

    @AfterTest
    fun tearDown() = runBlocking {
        executor.disconnect()
        Unit
    }

    @Test
    fun testUpdateQuery() = runBlocking {
        val delayedResult = lazyUpdate(Users)
                .changeOn {
                    (Users.firstName equal "bob") and
                            (Users.lastName equal "dean")
                }
                .mutateTo {
                    Users.firstName TO "Sausage"
                    Users.lastName TO "Man"
                }
        val query = delayedResult.query.buildSQLString(delayedResult.sqlBuilder)
        val expected = """
            update jasync_example.users SET first_name = 'Sausage',last_name = 'Man' where users.first_name = 'bob' AND users.last_name = 'dean'
        """.trimIndent()
        println(query)
        println(expected)
        assertTrue { query == expected }
    }

    @Test
    fun testUpdate() = runBlocking {
        val originalUserQuery = lazySelect(Users).filter {
            (Users.firstName equal "bob") and
                    (Users.lastName equal "dean")
        }
        val delayedOriginalUser = originalUserQuery.defer(executor)
        val delayedResult = lazyUpdate(Users)
                .changeOn {
                    (Users.firstName equal "bob") and
                            (Users.lastName equal "dean")
                }
                .mutateTo {
                    Users.firstName TO "Sausage"
                    Users.lastName TO "Man"
                }
                .save(executor)
        delayedResult.fetch()
        val delayedOldUser = originalUserQuery.defer(executor)
        val delayedNewUser = lazySelect(Users).filter {
            (Users.firstName equal "Sausage") and (Users.lastName equal "Man")
        }.defer(executor)
        try {
            delayedOldUser.fetch()
            assertTrue { false }
        } catch (e: EmptyResultSet) {
            assertTrue { true }
        }
        val newUser = delayedNewUser.fetch()
        val originalUser = delayedOriginalUser.fetch()
        assertTrue {
            originalUser.id == newUser.id &&
                    newUser.firstName == "Sausage" &&
                    newUser.lastName == "Man"
        }
    }

    @Test
    fun testUpdateReturnRecord() = runBlocking {
        val newBio = "Bio updated via return record"
        val delayedUpdate = lazyUpdate(UserProfile)
                .changeOn { UserProfile.email equal "jdoe@google.com" }
                .mutateTo {
                    UserProfile.bio TO newBio
                }
                .returning()
                .defer(executor)
                .fetch()
        assertTrue { delayedUpdate.size == 1 }
        val updatedBio = delayedUpdate.first()
        assertTrue { updatedBio.bio == newBio }
    }

    @Test
    fun testUpdateReturnCustom() = runBlocking {
        val newBio = "Bio updated via return custom"
        val delayedUpdate = lazyUpdate(UserProfile)
                .changeOn { UserProfile.email equal "jbob@google.com" }
                .mutateTo {
                    UserProfile.bio TO newBio
                }
                .returning(UserProfile.bio)
                .defer(executor)
                .fetch()
        assertTrue { delayedUpdate.size == 1 }
        val updatedBio = delayedUpdate.first()
        assertTrue { updatedBio.containsKey(UserProfile.bio.columnName) }
        assertTrue { updatedBio[UserProfile.bio.columnName] == newBio }
    }
}
