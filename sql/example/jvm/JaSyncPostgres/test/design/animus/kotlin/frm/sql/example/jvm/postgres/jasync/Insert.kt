package design.animus.kotlin.frm.sql.example.jvm.postgres.jasync

import design.animus.kotlin.frm.sql.example.jvm.postgres.generated.database.postgres.tables.Users
import design.animus.kotlin.frm.sql.executors.jvm.jasync.JaSyncPostgresExecutor
import design.animus.kotlin.frm.sql.query.postgres.frm.lazyInsert
import design.animus.kotlin.frm.sql.query.postgres.frm.lazySelect
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.test.assertTrue

class InsertFRMTest {
    lateinit var executor: JaSyncPostgresExecutor

    @Before
    fun initialize() {
        runBlocking {
            executor = JaSyncPostgresExecutor()
            executor.connect(FRMTestConfig)
        }
    }

    @After
    fun tearDown() = runBlocking {
        executor.disconnect()
        Unit
    }

    @Test
    fun testInsertRecord() = runBlocking {
        val rsp = lazyInsert(Users)
                .add(
                        listOf(
                                listOf(Users.firstName to "Jonnny", Users.lastName to "Johnson"),
                                listOf(Users.firstName to "Jimmy", Users.lastName to "Nashorn")
                        )
                )
                .save(executor)
                .fetch()
        val newUserOne = lazySelect(Users).filter {
            (Users.firstName equal "Jonnny") and (Users.lastName equal "Johnson")
        }.deferMultiple(executor)
        val newUserTwo = lazySelect(Users).filter {
            (Users.firstName equal "Jimmy") and (Users.lastName equal "Nashorn")
        }.deferMultiple(executor)
        val userOneList = newUserOne.fetch().size
        val userTwoList = newUserTwo.fetch().size
        assertTrue { userOneList == 1 }
        assertTrue { userTwoList == 1 }
    }

    @Test
    fun testInsertReturnMultipleId() = runBlocking {
        val delayedInsert = lazyInsert(Users)
                .add(
                        listOf(
                                listOf(Users.firstName to "Return", Users.lastName to "Johnson"),
                                listOf(Users.firstName to "Return", Users.lastName to "Nashorn")
                        )
                )
                .returning(Users.id)
                .saveAndReturn(executor) { users ->
                    assertTrue { users.size == 2 }
                    users.forEach { possibleUser ->
                        assertTrue { possibleUser.containsKey(Users.id.columnName) }
                    }
                }
        val x = delayedInsert.fetch()
    }

    @Test
    fun testInsertReturnId() = runBlocking {
        val delayedInsert = lazyInsert(Users)
                .addOne(listOf(Users.firstName to "Return", Users.lastName to "Id"))
                .returning(Users.id)
                .saveAndReturn(executor) { possibleUser ->
                    assertTrue { possibleUser.containsKey(Users.id.columnName) }
                }
    }

    @Test
    fun testInsertReturnMultipleRecord() = runBlocking {
        val delayedInsert = lazyInsert(Users)
                .add(
                        listOf(
                                listOf(Users.firstName to "Return", Users.lastName to "Johnson"),
                                listOf(Users.firstName to "Return", Users.lastName to "Nashorn")
                        )
                )
                .returning()
                .saveAndReturn(executor) { users ->
                    assertTrue { users.size == 2 }
                    users.forEach { user ->
                        assertTrue { user.id != null }
                        assertTrue { user.firstName == "Return" }
                        assertTrue { user.lastName == "Johnson" || user.lastName == "Nashorn" }
                    }
                }
        val x = delayedInsert.fetch()
    }

    @Test
    fun testInsertReturnRecord() = runBlocking {
        val delayedInsert = lazyInsert(Users)
                .addOne(listOf(Users.firstName to "Return", Users.lastName to "Id"))
                .returning()
                .saveAndReturn(executor) { user ->
                    assertTrue { user.id != null }
                    assertTrue { user.firstName == "Return" }
                    assertTrue { user.lastName == "Id" }
                }
    }

}
