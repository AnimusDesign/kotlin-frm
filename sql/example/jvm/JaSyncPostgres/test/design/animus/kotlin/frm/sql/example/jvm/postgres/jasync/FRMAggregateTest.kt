package design.animus.kotlin.frm.sql.example.jvm.postgres.jasync

import design.animus.kotlin.frm.sql.example.jvm.postgres.generated.database.postgres.tables.Inventory
import design.animus.kotlin.frm.sql.example.jvm.postgres.generated.database.postgres.tables.UserOrders
import design.animus.kotlin.frm.sql.example.jvm.postgres.generated.database.postgres.tables.Users
import design.animus.kotlin.frm.sql.executors.jvm.jasync.JaSyncPostgresExecutor
import design.animus.kotlin.frm.sql.query.postgres.frm.lazySelectJoin
import design.animus.kotlin.frm.sql.query.postgres.query.aggregates.jsonAgg
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import org.junit.After
import org.junit.Before
import org.junit.Test

class FRMAggregateTest {
    lateinit var executor: JaSyncPostgresExecutor
    val json = Json(JsonConfiguration.Stable)

    @Before
    fun initialize() = runBlocking {
        executor = JaSyncPostgresExecutor()
        executor.connect(FRMTestConfig)
        Unit
    }

    @After
    fun tearDown() = runBlocking {
        executor.disconnect()
        Unit
    }

    @Test
    fun testJsonAggregate() = runBlocking {
        val delayedQuery = lazySelectJoin(Users)
            .withColumns(Users.id, Users.firstName, Users.lastName,
                jsonAgg(name = "orders") {
                    jsonObj {
                        "orderPlaced" to UserOrders.orderPlaced
                        "inventory" to jsonObj {
                            "id" to Inventory.id
                            "name" to Inventory.name
                            "price" to Inventory.price
                        }
                    }
                }
            )
            .join(UserOrders) { UserOrders.userId equal Users.id }
            .join(Inventory) { Inventory.id equal UserOrders.inventoryId }
            .groupBy(Users.id, Users.firstName, Users.lastName)
            .map(executor) {
				it
            }
        delayedQuery.fetch()
        Unit
    }
}

