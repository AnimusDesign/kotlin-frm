package design.animus.kotlin.frm.sql.example.jvm.postgres

import design.animus.kotlin.frm.sql.query.common.query.IDatabase
import design.animus.kotlin.frm.sql.query.postgres.query.IPostgresDatabase

//import design.animus.kotlin.frm.sql.example.jvm.postgres.generated.database.postgres.tables.Users
//import design.animus.kotlin.frm.sql.query.common.SQLConditionalBuilder
//import design.animus.kotlin.frm.sql.query.common.query.IDatabase
//import design.animus.kotlin.frm.sql.query.common.query.ITable
//import design.animus.kotlin.frm.sql.query.common.query.SQLVerbs
//import design.animus.kotlin.frm.sql.query.common.query.Table
//import design.animus.kotlin.frm.sql.query.common.query.type.SelectQuery
//import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
//import kotlinx.serialization.ContextualSerialization
//import kotlinx.serialization.Serializable
//import javax.swing.text.DefaultEditorKit
//
//@Serializable
//data class JsonInventory(
//        @ContextualSerialization
//        val id: String,
//        val name: String,
//        @ContextualSerialization
//        val price: String
//)
//
//@Serializable
//data class JsonOrder(
//        val orderPlaced: String,
//        val inventory: JsonInventory
//)

suspend fun main() {
    println("Hello")
    println(IDatabase::class)
    println(IPostgresDatabase::class)
}