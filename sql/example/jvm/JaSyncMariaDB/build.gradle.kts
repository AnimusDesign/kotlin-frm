import design.animus.kotlin.frm.Versions.Dependencies

apply("$rootDir/versions.gradle.kts")
plugins {
    kotlin("jvm")
    id("org.jetbrains.kotlin.plugin.serialization")
    id("design.animus.kotlin.frm.sql.schema.mariadb") version "0.1.5-SNAPSHOT"
    id("org.flywaydb.flyway") version "6.1.3"
    jacoco
}

kotlin {
    target {

    }
    sourceSets {
        main {
            kotlin.setSrcDirs(
                    mutableListOf("main", "generated")
            )
            resources.srcDir("main/resources")
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
                implementation(project(":sql:query:query-common:"))
                implementation(project(":sql:query:query-mariadb:"))
                implementation(project(":sql:executors:jvm:jasync-mysql:"))
                implementation(project(":sql:executors:jvm:jasync-base:"))
                implementation("io.github.microutils:kotlin-logging-jvm:${Dependencies.kotlinLogging}")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Dependencies.coroutine}")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime:${Dependencies.serialization}")
                implementation("org.mariadb.jdbc:mariadb-java-client:${Dependencies.mariaDBDriver}")
                implementation("design.animus.kotlin.mp:datatypes-jvm:${Dependencies.MPDataTypes}")
            }
        }
        test {
            kotlin.srcDir("test")
            resources.srcDir("test/resources")
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
                implementation(kotlin("test-junit"))
                implementation(kotlin("test"))
                implementation(project(":sql:query:query-common:"))
                implementation(project(":sql:query:query-mariadb:"))
                implementation("com.github.jasync-sql:jasync-mysql:${Dependencies.jasync}")

            }
        }
    }
}

val dbHost = System.getenv("myDataBaseHost") ?: "localhost"
val db = "example_jasync"
val dbPort = (System.getenv("myDataBasePort") ?: "3306").toInt()
val dbUser = System.getenv("myDataBaseUser") ?: "mariadb"
val dbPassword = System.getenv("myDataBasePassword") ?: "mariadb"

flyway {
    url = "jdbc:mariadb://$dbHost/$db?user=$dbUser&password=$dbPassword"
    locations = arrayOf("filesystem:$projectDir/main/resources")
    baselineOnMigrate = true
}

MariaDBGeneratorConfig {
    dataBases = listOf(db)
    dataBaseHost = dbHost
    dataBaseUser = dbUser
    dataBasePassword = dbPassword
    dataBasePort = dbPort
    namespace = "design.animus.kotlin.frm.sql.example.jvm.mysql.generated"
    outputDir = "$projectDir/generated"
    platforms = setOf(design.animus.kotlin.frm.sql.query.common.JVM)
    excludeTables = setOf("flyway_schema_history")
}

tasks.withType<Test>().configureEach {
    useJUnit()
    reports.junitXml.isEnabled = true
}