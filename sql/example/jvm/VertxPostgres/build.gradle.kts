import design.animus.kotlin.frm.Versions.Dependencies

plugins {
    kotlin("jvm")
    id("org.jetbrains.kotlin.plugin.serialization")
    id("design.animus.kotlin.frm.sql.schema.postgresql") version "0.1.5-SNAPSHOT"
    id("org.flywaydb.flyway") version "6.0.7"
}

tasks {
    withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions.jvmTarget = "1.8"
    }
}
kotlin {
    target {

    }
    sourceSets {
        main {
            kotlin.setSrcDirs(
                    mutableListOf("main", "generated")
            )
            resources.srcDir("main/resources")
            dependencies {
                implementation(project(":sql:query:query-common:"))
                implementation(project(":sql:query:query-postgresql:"))
                implementation(project(":sql:executors:jvm:vertx-postgres:"))
                implementation(project(":sql:executors:jvm:vertx-base:"))
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Dependencies.coroutine}")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime:${Dependencies.serialization}")
                implementation("org.jetbrains.kotlin:kotlin-reflect:${Dependencies.kotlin}")
                implementation("io.github.microutils:kotlin-logging-jvm:${Dependencies.kotlinLogging}")
                implementation("io.vertx:vertx-core:${Dependencies.vertx}")
                implementation("io.vertx:vertx-lang-kotlin:${Dependencies.vertx}")
                implementation("io.vertx:vertx-lang-kotlin-coroutines:${Dependencies.vertx}")
                implementation("io.vertx:vertx-pg-client:${Dependencies.vertx}")
                implementation("design.animus.kotlin.mp:datatypes-jvm:${Dependencies.MPDataTypes}")
            }
        }
        test {
            kotlin.srcDir("test")
            resources.srcDir("test/resources")
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
                implementation(kotlin("test-junit"))
                implementation(kotlin("test"))
                implementation(project(":sql:query:query-common:"))
                implementation(project(":sql:query:query-postgresql:"))
                implementation("com.github.jasync-sql:jasync-postgresql:${Dependencies.jasync}")
            }
        }
    }
}

val dbHost = System.getenv("pgDataBaseHost") ?: "localhost"
val db = System.getenv("pgDataBase") ?: "postgres"
val dbPort = (System.getenv("pgDataBasePort") ?: "5432").toInt()
val dbUser = System.getenv("pgDataBaseUser") ?: "postgres"
val dbPassword = System.getenv("pgDataBasePassword") ?: "postgres"

flyway {
    url = "jdbc:postgresql://$dbHost/$db?user=$dbUser&password=$dbPassword"
    schemas = arrayOf("vertx_example")
    locations = arrayOf("filesystem:$projectDir/main/resources")
}

PostgresGeneratorConfig {
    dataBases = listOf(db)
    dataBaseHost = dbHost
    dataBaseUser = dbUser
    dataBasePassword = dbPassword
    dataBasePort = dbPort
    namespace = "design.animus.kotlin.frm.sql.example.jvm.vertx.postgres.generated"
    outputDir = "$projectDir/generated"
    dataBaseSchema = "vertx_example"
    platforms = setOf(design.animus.kotlin.frm.sql.query.common.JVM)
    excludeTables = setOf("flyway_schema_history")
}

tasks.withType<Test>().configureEach {
    useJUnit()
    reports.junitXml.isEnabled = true
}