package design.animus.kotlin.frm.sql.query.common.schema

import design.animus.kotlin.frm.sql.query.common.query.Field
import design.animus.kotlin.frm.sql.query.common.query.ITable
import design.animus.kotlin.frm.sql.query.common.query.LateField
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecordByMap


object Profile : ITable<ExampleIDatabase, ProfileRecord> {
    override val reflectedDatabase = ExampleIDatabase
    override val reflectedTableName = "user_profile"
    override val reflectedRecordClass = ProfileRecord::class
    override val reflectedColumns = listOf("id", "email")
    override val reflectedTable = this
    val id: Field<Long, ExampleIDatabase, ProfileRecord, Profile> = object :
        Field<Long, ExampleIDatabase, ProfileRecord, Profile>(
            "id",
            "bigint",
            reflectedDatabase.database,
            reflectedTableName

        ) {}
    val email: Field<String, ExampleIDatabase, ProfileRecord, Profile> = object :
        Field<String, ExampleIDatabase, ProfileRecord, Profile>(
            "email",
            "varchar",
            reflectedDatabase.database,
            reflectedTableName
        ) {}
    val lazyId: LateField<Long, ExampleIDatabase, ProfileRecord, Profile> = UserProfileLazyId

    val lazyEmail: LateField<String?, ExampleIDatabase, ProfileRecord, Profile> = UserProfileLazyEmail
}

data class ProfileRecord(val map: Map<String, Any>) : ADatabaseRecordByMap(map) {
    val id: Long by map
    val email: String by map
}

object UserProfileLazyId : LateField<Long, ExampleIDatabase, ProfileRecord, Profile>(
    "Int", "id",
    "integer", "example", "user_profile"
)

object UserProfileLazyEmail : LateField<String?, ExampleIDatabase, ProfileRecord, Profile>(
    "String",
    "email", "character varying", "example", "user_profile"
)