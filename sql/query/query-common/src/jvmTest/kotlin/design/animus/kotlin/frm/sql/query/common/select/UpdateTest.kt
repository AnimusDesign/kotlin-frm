package design.animus.kotlin.frm.sql.query.common.select


import design.animus.kotlin.frm.sql.query.common.builder.query
import design.animus.kotlin.frm.sql.query.common.builder.sqlQuery
import design.animus.kotlin.frm.sql.query.common.query.Fields
import design.animus.kotlin.frm.sql.query.common.query.type.UpdateQuery
import design.animus.kotlin.frm.sql.query.common.schema.ExampleIDatabase
import design.animus.kotlin.frm.sql.query.common.schema.Users
import design.animus.kotlin.frm.sql.query.common.schema.UsersRecord
import kotlinx.coroutines.runBlocking
import org.junit.Test
import kotlin.test.assertTrue

@Suppress("UNCHECKED_CAST")
class UpdateTest {
    @Test
    fun updateOneField() = runBlocking {
        val query = sqlQuery<ExampleIDatabase, UsersRecord, Users> {
            update(Users) set {
                Users.firstName TO "Bob"
                Users.lastName TO "Smith"
            } where {
                (Users.id equal 1)
            }
        }
        val sql = query.buildSQLString()
        assertTrue {
            sql ==
                    "update users SET users.first_name = 'Bob',users.last_name = 'Smith' where users.id = 1"
        }

    }

    @Test
    fun updateOneFieldAndReturnId() = runBlocking {
        val query = Users.query {
            update(Users) set {
                Users.firstName TO "Bob"
                Users.lastName TO "Smith"
            } where {
                (Users.id equal 1)
            } returning (listOf(Users.id) as List<Fields<Any?, ExampleIDatabase, UsersRecord, Users >>)
        }
        val sql = query.buildSQLString()
        val expected = "update users SET users.first_name = 'Bob',users.last_name = 'Smith' where users.id = 1 returning users.id"
        println("SQL: $sql")
        println("Expected: $expected")
        assertTrue {
            sql == expected
        }
    }

    @Test
    fun updateOneFieldAndReturnMultipleFields() = runBlocking {
        val query = Users.query {
            update(Users) set {
                Users.firstName TO "Bob"
                Users.lastName TO "Smith"
            } where {
                (Users.id equal 1)
            } returning (listOf(Users.id, Users.firstName, Users.lastName) as List<Fields<Any?, ExampleIDatabase, UsersRecord, Users >>)
        }
        val sql = query.buildSQLString()
        val expected = "update users SET users.first_name = 'Bob',users.last_name = 'Smith' where users.id = 1 returning users.id,users.first_name,users.last_name"
        println("SQL: $sql")
        println("Expected: $expected")
        assertTrue {
            sql == expected
        }
    }

    @Test
    fun updateOneFieldAndReturnAll() = runBlocking {
        val query = Users.query {
            update(Users) set {
                Users.firstName TO "Bob"
                Users.lastName TO "Smith"
            } where {
                (Users.id equal 1)
            } returning listOf()
        }
        val sql = query.buildSQLString()
        val expected = "update users SET users.first_name = 'Bob',users.last_name = 'Smith' where users.id = 1 returning *"
        println("SQL: $sql")
        println("Expected: $expected")
        assertTrue {
            sql == expected
        }
    }
}
