package design.animus.kotlin.frm.sql.query.common.schema

import design.animus.kotlin.frm.sql.query.common.query.Field
import design.animus.kotlin.frm.sql.query.common.query.ITable
import design.animus.kotlin.frm.sql.query.common.query.LateField
import design.animus.kotlin.frm.sql.query.common.query.PrimaryKey
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecordByMap

object Users : ITable<ExampleIDatabase, UsersRecord> {
    override val reflectedRecordClass = UsersRecord::class
    override val reflectedTable = this
    override val reflectedTableName = "users"
    override val reflectedDatabase = ExampleIDatabase
    override val reflectedColumns = listOf("id", "first_name", "last_name")
    val id: PrimaryKey<Long, ExampleIDatabase, UsersRecord, Users> = object :
        PrimaryKey<Long, ExampleIDatabase, UsersRecord, Users>(
            "id",
            "bigint",
            reflectedDatabase.database,
            reflectedTableName
        ) {}
    val firstName: Field<String, ExampleIDatabase, UsersRecord, Users> = object :
        Field<String, ExampleIDatabase, UsersRecord, Users>(
            "first_name",
            "varchar(24)",
            reflectedDatabase.database,
            reflectedTableName
        ) {}
    val lastName: Field<String, ExampleIDatabase, UsersRecord, Users> = object :
        Field<String, ExampleIDatabase, UsersRecord, Users>(
            "last_name",
            "varchar(24)",
            reflectedDatabase.database,
            reflectedTableName
        ) {}
    val lazyId: LateField<Long, ExampleIDatabase, UsersRecord, Users> = UsersLazyId

    val lazyFirstName: LateField<String, ExampleIDatabase, UsersRecord, Users> = UsersLazyFirstName

    val lazyLastName: LateField<String, ExampleIDatabase, UsersRecord, Users> = UsersLazyLastName
}


data class UsersRecord(
    val map: Map<String, Any>
) : ADatabaseRecordByMap(map) {
    val id: Long by map
    val firstName by map
}

object UsersLazyId : LateField<Long, ExampleIDatabase, UsersRecord, Users>(
    "Int", "id", "integer",
    "jasync_example", "users"
)

object UsersLazyFirstName : LateField<String, ExampleIDatabase, UsersRecord, Users>(
    "String", "first_name",
    "character varying", "jasync_example", "users"
)

object UsersLazyLastName : LateField<String, ExampleIDatabase, UsersRecord, Users>(
    "String", "last_name",
    "character varying", "jasync_example", "users"
)