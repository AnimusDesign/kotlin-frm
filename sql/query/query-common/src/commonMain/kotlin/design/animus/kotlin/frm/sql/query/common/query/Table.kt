package design.animus.kotlin.frm.sql.query.common.query

import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
import kotlin.reflect.KClass


sealed class QueryTable
data class Table<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(val table: T) : QueryTable()
data class NamedTable<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
    val table: T,
    val name: String
) : QueryTable()

interface ITable<D : IDatabase, R : ADatabaseRecord> {
    val reflectedTableName: String
    val reflectedDatabase: D
    val reflectedColumns: List<String>
    val reflectedRecordClass: KClass<R>
    val reflectedTable: ITable<D, R>
}

infix fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> T.AS(value: String) = NamedTable(this, value)
typealias TableNamePair = Pair<SchemaTableName, FriendlyTableName>
typealias SchemaTableName = String
typealias FriendlyTableName = String
