package design.animus.kotlin.frm.sql.query.common

import design.animus.kotlin.frm.sql.query.common.ClassName as SQLClassName

abstract class SQLTypeProvider(
    val dataBaseInterface: SQLClassName,
    val tableInterface: SQLClassName
) {
    abstract suspend fun getTypeByName(type: SQLType): ISQLType
}

suspend fun getRawType(
    sqlType: SQLType,
    platform: Platform,
    typeProvider: SQLTypeProvider,
    customTypes: CustomTypes
): SQLClassName {
    val customType = customTypes.filter { it.sqlType == sqlType }
    return when {
        customType.isNotEmpty() -> {
            val t = customType.first()
            println(t)
            SQLClassName(t.packageName, t.className)
        }
        else -> {
            when (platform) {
                is JVM -> typeProvider.getTypeByName(sqlType).jvmType
                is JavaScript -> typeProvider.getTypeByName(sqlType).jsType
                is Native -> typeProvider.getTypeByName(sqlType).nativeType
                is Common -> typeProvider.getTypeByName(sqlType).commonType
            }
        }
    }
}