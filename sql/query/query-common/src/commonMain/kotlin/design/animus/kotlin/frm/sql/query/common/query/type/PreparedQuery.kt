package design.animus.kotlin.frm.sql.query.common.query.type

import design.animus.functional.datatypes.option.Option
import design.animus.kotlin.frm.sql.executors.common.exceptions.LazyConditionTypeMismatch
import design.animus.kotlin.frm.sql.executors.common.exceptions.LazyConditionsNotEqualToArguments
import design.animus.kotlin.frm.sql.executors.common.logger
import design.animus.kotlin.frm.sql.query.common.builder.CommonSQLBuilder
import design.animus.kotlin.frm.sql.query.common.builder.ISQLBuilder
import design.animus.kotlin.frm.sql.query.common.builder.SQLQuery
import design.animus.kotlin.frm.sql.query.common.builder.magicPreparedKey
import design.animus.kotlin.frm.sql.query.common.frm.LazyInsertPair
import design.animus.kotlin.frm.sql.query.common.query.*
import design.animus.kotlin.frm.sql.query.common.query.verbs.ISelectQuery
import design.animus.kotlin.frm.sql.query.common.query.verbs.UpdateChange
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord

typealias BoundArguments<D, R, T> = List<Condition<Any?, D, R, T>>
typealias PreparedVariable = String

private suspend fun extractLazyConditions(conditions: List<ConditionalSteps>): List<ConditionalSteps> {
    val topLevelFields = conditions.filter {
        it is FieldComparison<*, *> && (it.fieldTwo is LateField<*, *, *, *>)
    }
    val nestedFields = conditions.filter {
        it is FieldComparison<*, *> && (it.fieldTwo is NestedQuery<*, *, *, *>)
    }.map {
        val nestedQuery = (it as FieldComparison<*, *>).fieldTwo as NestedQuery<*, *, *, *>
        when (val query = nestedQuery.query) {
            is SelectQuery -> extractLazyConditions(query.conditions)
            is UpdateQuery -> extractLazyConditions(query.conditions)
            is InsertQuery -> listOf()
            is SelectJoinQuery -> extractLazyConditions(query.conditions)
            is DeleteQuery -> extractLazyConditions(query.conditions)
        }
    }.flatten()
    return topLevelFields + nestedFields
}

@Suppress("UNCHECKED_CAST")
private suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> baseBind(
        conditions: List<ConditionalSteps>,
        vararg values: Any?
): List<Condition<Any?, D, R, T>> {
    val arguments = values.toList()
    val lazyConditions = extractLazyConditions(conditions)
    if (arguments.size != lazyConditions.size) throw LazyConditionsNotEqualToArguments(
            "Received ${arguments.size} arguments, but ${lazyConditions.size} conditions were expected.",
            lazyConditions, arguments
    )
    return lazyConditions.mapIndexed { idx, field ->
        val fieldComparison = field as FieldComparison<*, *>
        val lazyField = fieldComparison.fieldTwo as LateField<*, *, *, *>
        val argumentType = arguments[idx]!!::class.simpleName
        if (lazyField.simpleName != argumentType) throw LazyConditionTypeMismatch(
                "Argument $idx was expected to be ${lazyField.simpleName} but was $argumentType",
                idx, lazyField.simpleName, argumentType.toString()
        )
        Condition<Any?, D, R, T>(
                fieldComparison.fieldOne as Fields<Any?, D, R, T>,
                fieldComparison.operator, arguments[idx]
        )
    }
}

interface IPreparedBind<Return> {
    suspend fun bind(vararg values: Any?): Return
}

sealed class PreparedQuery<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, Return> :
        IPreparedQuery<D, R, T>, IPreparedBind<Return> {

    override suspend fun buildSQLString(variable: PreparedPlacedHolder): SQLQuery {
        val lockedQuery = this.toLockedQuery()
        val sqlQuery = lockedQuery.buildSQLString()
        val rsp = sqlBuilder.substitutePreparedVarWithExecutorVar(sqlQuery, variable)
        return rsp
    }

    private suspend fun toLockedQuery(): LockedQuery<D, R, T> = when (this) {
        is PreparedSelectQuery -> SelectQuery<D, R, T>(
                sqlBuilder, this.verb, this.columns, this.conditions, this.table, this.groupBy
        )
        is PreparedSelectJoinQuery -> SelectJoinQuery<D, R, T>(
                sqlBuilder, this.verb, this.columns, this.conditions, this.table, this.joinStatements, this.groupBy
        )
        is PreparedDeleteQuery -> DeleteQuery<D, R, T>(
                sqlBuilder, this.table, this.conditions, this.verb
        )
        is PreparedInsertQuery -> {
            val newItems = this.newItems.map { x ->
                x.map { (field, value) ->
                    Pair(field, magicPreparedKey)
                }
            }
            InsertQuery<D, R, T>(
                    this.sqlBuilder, this.verb, this.table, newItems, this.returning
            )
        }
        is PreparedUpdateQuery -> {
            UpdateQuery<D, R, T>(
                    this.sqlBuilder, this.verb, this.conditions, this.table, this.changes, this.returning
            )
        }
    }

    @Suppress("UNCHECKED_CAST")
    companion object {
        suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> fromLockedQuery(
                query: LockedQuery<D, R, T>,
                sqlBuilder: ISQLBuilder<D, R, T> = CommonSQLBuilder()
        ) = when (query) {
            is SelectQuery -> PreparedSelectQuery<D, R, T>(
                    sqlBuilder, query.verb, query.columns, query.conditions, query.table, query.groupBy
            )
            is UpdateQuery -> PreparedUpdateQuery<D, R, T>(
                    sqlBuilder, query.conditions, query.table, query.changes, query.verb, query.returningFields
            )
            is InsertQuery -> PreparedInsertQuery<D, R, T>(
                    query.newItems as List<LazyInsertPair<D, R, T>>, sqlBuilder, query.table, query.verb, query.returningFields
            )
            is SelectJoinQuery -> PreparedSelectJoinQuery<D, R, T>(
                    sqlBuilder,
                    query.table,
                    query.columns as List<Fields<*, D, R, T>>,
                    query.conditions,
                    query.joinStatements,
                    query.groupBy,
                    query.verb
            )
            is DeleteQuery -> PreparedDeleteQuery<D, R, T>(
                    sqlBuilder, query.table, query.conditions, query.verb
            )
        }
    }
}

data class PreparedSelectQuery<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        override val sqlBuilder: ISQLBuilder<D, R, T>,
        override val verb: SQLVerbs,
        override val columns: List<Fields<*, D, R, T>>,
        override val conditions: List<ConditionalSteps>,
        override val table: QueryTable,
        override val groupBy: List<Fields<*, *, *, *>>
) : ISelectQuery, PreparedQuery<D, R, T, List<ConditionalSteps>>() {

    override suspend fun bind(vararg values: Any?) =
            baseBind<D, R, T>(conditions, *values)
}

data class PreparedSelectJoinQuery<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        override val sqlBuilder: ISQLBuilder<D, R, T>,
        override val table: QueryTable,
        override val columns: List<Fields<*, D, R, T>> = listOf(),
        override val conditions: List<ConditionalSteps> = listOf(),
        override val joinStatements: List<JoinType> = listOf(),
        override val groupBy: List<Fields<*, *, *, *>> = listOf(),
        override val verb: SQLVerbs = SQLVerbs.SELECTJOIN
) : IPreparedSelectJoinQuery<D, R, T>, PreparedQuery<D, R, T, BoundArguments<D, R, T>>() {

    override suspend fun bind(vararg values: Any?): BoundArguments<D, R, T> {
        val lazyJoinStatements = extractLazyConditions(joinStatements.map { it.condition }.flatten())
        val lazyConditions = extractLazyConditions(conditions)
        if ((lazyConditions.size + lazyJoinStatements.size) != values.size) throw LazyConditionsNotEqualToArguments(
                "Expecting ${lazyConditions.size} where conditions and ${lazyJoinStatements.size} lazy join conditions, for a total of ${lazyConditions.size + lazyJoinStatements.size} but got ${values.size} arguments",
                (lazyJoinStatements + lazyConditions), values.toList()
        )
        logger.debug { "Found ${lazyJoinStatements.size} lazy join statements." }
        val arguments = values.toList()
        val joinArgs = if (lazyJoinStatements.isNotEmpty()) {
            val joinArguments = arguments.subList(0, lazyJoinStatements.size)
            baseBind<D, R, T>(lazyJoinStatements, *joinArguments.toTypedArray())
        } else {
            listOf()
        }
        val whereArgs = if (lazyJoinStatements.isEmpty()) {
            baseBind<D, R, T>(lazyConditions, *arguments.toTypedArray())
        } else {
            val whereArguments = arguments.subList(lazyJoinStatements.size, arguments.size)
            baseBind<D, R, T>(lazyConditions, *whereArguments.toTypedArray())
        }
        return (joinArgs + whereArgs)
    }
}

data class PreparedDeleteQuery<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        override val sqlBuilder: ISQLBuilder<D, R, T>,
        override val table: QueryTable,
        override val conditions: List<ConditionalSteps> = listOf(),
        override val verb: SQLVerbs = SQLVerbs.DELETE
) : IPreparedDeleteQuery<D, R, T>, PreparedQuery<D, R, T, BoundArguments<D, R, T>>() {
    override suspend fun bind(vararg values: Any?): BoundArguments<D, R, T> = baseBind(conditions, *values)
}

data class PreparedInsertQuery<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        override val newItems: List<LazyInsertPair<D, R, T>>,
        override val sqlBuilder: ISQLBuilder<D, R, T>,
        override val table: QueryTable,
        override val verb: SQLVerbs = SQLVerbs.INSERT,
        override val returning: Option<List<Fields<Any?, D, R, T>>> = Option.None()
) : IPreparedInsertQuery<D, R, T>, PreparedQuery<D, R, T, BoundArguments<D, R, T>>() {
    private suspend fun bindInsertPairs(
            lazyNewItems: LazyInsertPair<D, R, T>,
            vararg values: Any?
    ): BoundArguments<D, R, T> {
        val arguments = values.toList()
        return lazyNewItems.mapIndexed { idx, item ->
            val (field, lateField) = item
            val argumentType = arguments[idx]!!::class.simpleName
            if (lateField.simpleName != argumentType) throw LazyConditionTypeMismatch(
                    "Argument $idx was expected to be ${lateField.simpleName} but was $argumentType",
                    idx, lateField.simpleName, argumentType.toString()
            )
            Condition(field, SQLConditionalOperator.Equal, arguments[idx])
        }
    }

    override suspend fun bind(vararg values: Any?): BoundArguments<D, R, T> {
        return bindInsertPairs(newItems.first(), *values)
    }

    suspend fun bulkBind(vararg values: List<Any?>): List<BoundArguments<D, R, T>> {
        return newItems.mapIndexed { idx, set ->
            bindInsertPairs(set, *values[idx].toTypedArray())
        }
    }
}

data class PreparedUpdateQuery<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        override val sqlBuilder: ISQLBuilder<D, R, T>,
        override val conditions: List<ConditionalSteps>,
        override val table: QueryTable,
        override val changes: List<UpdateChange<*, D, R, T>>,
        override val verb: SQLVerbs = SQLVerbs.UPDATE,
        override val returning: Option<List<Fields<Any?, D, R, T>>> = Option.None()
) : IPreparedUpdateQuery<D, R, T>, PreparedQuery<D, R, T, BoundArguments<D, R, T>>() {
    @Suppress("UNCHECKED_CAST")
    override suspend fun bind(vararg values: Any?): BoundArguments<D, R, T> {
        val arguments = values.toList()
        val lazyChanges = changes.mapIndexedNotNull { index, change ->
            if (change.lateField != null) {
                if (change.lateField.simpleName != arguments[index]!!::class.simpleName) throw LazyConditionTypeMismatch(
                        "Field: ${change.field} with Late Field: ${change.lateField} did not match given type ${arguments[index]!!::class.simpleName}",
                        index, change.lateField.simpleName, arguments[index]!!::class.simpleName ?: ""
                )
                FieldComparison<Any?, D>(
                        change.field as Field<Any?, D, R, T>, change.lateField as Fields<Any?, D, *, *>,
                        SQLConditionalOperator.Equal
                )
            } else {
                null
            }
        }
        return baseBind<D, R, T>(lazyChanges + conditions, *values)
    }
}
