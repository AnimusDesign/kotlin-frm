package design.animus.kotlin.frm.sql.executors.common

import kotlin.reflect.KClass

sealed class ExpectedResponses

interface ICustomRecord<TargetClass : Any> {
    val targetClass: KClass<TargetClass>
}

interface IListOfCustomRecord<TargetClass : Any> {
    val targetClass: KClass<TargetClass>
}

object SingleOfRecord : ExpectedResponses()
object ListOfRecord : ExpectedResponses()
object HashMap : ExpectedResponses()
object ListOfHashMap : ExpectedResponses()
data class CustomRecord<TargetClass : Any>(override val targetClass: KClass<TargetClass>) : ICustomRecord<TargetClass>, ExpectedResponses()
data class ListOfCustomRecord<TargetClass : Any>(override val targetClass: KClass<TargetClass>) : IListOfCustomRecord<TargetClass>, ExpectedResponses()
object Save : ExpectedResponses()
object SaveAndReturn : ExpectedResponses()
object SaveAndReturnMultiple : ExpectedResponses()
object SaveAndReturnRecord : ExpectedResponses()
object SaveAndReturnListOfRecord : ExpectedResponses()
data class SaveAndReturnWithCustomRecord<TargetClass : Any>(override val targetClass: KClass<TargetClass>) : ICustomRecord<TargetClass>, ExpectedResponses()
data class SaveAndReturnWithCustomRecordMultiple<TargetClass : Any>(override val targetClass: KClass<TargetClass>) : IListOfCustomRecord<TargetClass>, ExpectedResponses()
object Delete : ExpectedResponses()
object DirectOfRecord : ExpectedResponses()
object DirectListOfRecord : ExpectedResponses()