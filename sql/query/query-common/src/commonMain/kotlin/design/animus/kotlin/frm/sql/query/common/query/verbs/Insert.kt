package design.animus.kotlin.frm.sql.query.common.query.verbs

import design.animus.functional.datatypes.option.Option
import design.animus.kotlin.frm.sql.query.common.frm.InsertPair
import design.animus.kotlin.frm.sql.query.common.query.Fields
import design.animus.kotlin.frm.sql.query.common.query.IDatabase
import design.animus.kotlin.frm.sql.query.common.query.ITable
import design.animus.kotlin.frm.sql.query.common.query.LateField
import design.animus.kotlin.frm.sql.query.common.query.type.ISaveQuery
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord

interface IMutableInsertQuery

interface IInsertQuery<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> : ISaveQuery<D, R, T> {
    val newItems: List<InsertPair<D, R, T>>
    val returningFields: Option<List<Fields<Any?, D, R, T>>>
}

@Suppress("UNCHECKED_CAST")
class InsertBuilder<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> {
    val newItems = mutableListOf<Pair<Fields<Any?, D, R, T>, Any?>>()
    val preparedNewItems = mutableListOf<
            Pair<
                    Fields<Any?, D, R, T>,
                    LateField<Any?, D, R, T>>
            >()

    suspend infix fun <C : Any?> Fields<C, D, R, T>.to(value: C) {
        newItems.add(Pair(this, value) as Pair<Fields<Any?, D, R, T>, Any?>)
    }

    suspend infix fun <C : Any?> Fields<C, D, R, T>.to(value: LateField<C, D, R, T>) {
        preparedNewItems.add(Pair(this, value) as Pair<Fields<Any?, D, R, T>, LateField<Any?, D, R, T>>)
    }
}