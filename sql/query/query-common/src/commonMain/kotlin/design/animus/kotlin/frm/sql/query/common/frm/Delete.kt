package design.animus.kotlin.frm.sql.query.common.frm

import design.animus.kotlin.frm.sql.executors.common.IAsyncExecutor
import design.animus.kotlin.frm.sql.executors.common.IExecutorConfig
import design.animus.kotlin.frm.sql.query.common.SQLConditionalBuilder
import design.animus.kotlin.frm.sql.query.common.builder.CommonSQLBuilder
import design.animus.kotlin.frm.sql.query.common.builder.ISQLBuilder
import design.animus.kotlin.frm.sql.query.common.query.IDatabase
import design.animus.kotlin.frm.sql.query.common.query.ITable
import design.animus.kotlin.frm.sql.query.common.query.Table
import design.animus.kotlin.frm.sql.query.common.query.type.DeleteMutableQuery
import design.animus.kotlin.frm.sql.query.common.query.type.DeleteQuery
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
import mu.KotlinLogging

class DeleteFRM<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
    val table: T,
    val sqlBuilder: ISQLBuilder<D, R, T>
) : IFRM<DeleteMutableQuery<D, R, T>> {
    override val logger = KotlinLogging.logger {}
    override var query = DeleteMutableQuery<D, R, T>(listOf())


    suspend fun deleteOn(condition: suspend SQLConditionalBuilder<D,R,T>.() -> Unit): DeleteFRM<D, R, T> {
        val state = SQLConditionalBuilder<D,R,T>()
        state.condition()
        query = query.copy(conditions = state.conditions).apply { table = Table(this@DeleteFRM.table) }
        return this
    }

    suspend fun <C : IExecutorConfig, RSLT> remove(asyncExecutor: IAsyncExecutor<C, RSLT>) =
        asyncExecutor.delete(query.lockQuery(sqlBuilder) as DeleteQuery)
}

suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>
        lazyDelete(
    table: T,
    sqlBuilder: ISQLBuilder<D, R, T> = CommonSQLBuilder<D, R, T>()
): DeleteFRM<D, R, T> = DeleteFRM<D, R, T>(table, sqlBuilder)