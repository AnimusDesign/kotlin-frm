package design.animus.kotlin.frm.sql.query.common.frm.prepare

import design.animus.kotlin.frm.sql.executors.common.*
import design.animus.kotlin.frm.sql.query.common.SQLConditionalBuilder
import design.animus.kotlin.frm.sql.query.common.builder.CommonSQLBuilder
import design.animus.kotlin.frm.sql.query.common.builder.ISQLBuilder
import design.animus.kotlin.frm.sql.query.common.frm.IFRM
import design.animus.kotlin.frm.sql.query.common.query.*
import design.animus.kotlin.frm.sql.query.common.query.type.PreparedSelectQuery
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
import kotlinx.coroutines.Dispatchers
import mu.KLogger
import mu.KotlinLogging
import kotlin.coroutines.CoroutineContext

class PreparedSelectFRMCustom<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        var query: PreparedSelectQuery<D, R, T>,
        val table: T,
        val sqlBuilder: ISQLBuilder<D, R, T>,
        val logger: KLogger = KotlinLogging.logger {}
) {
    suspend fun filter(block: suspend SQLConditionalBuilder<D, R, T>.() -> Unit): PreparedSelectFRMCustom<D, R, T> {
        val table = this
        val state = SQLConditionalBuilder<D, R, T>()
        state.block()
        query = query.copy(conditions = state.conditions)
        return this
    }

    suspend fun <C : IExecutorConfig, RSLT, RSP> mapOne(
            executor: IAsyncExecutor<C, RSLT>,
            vararg values: Any,
            ctx: CoroutineContext = Dispatchers.Default,
            block: suspend (Map<String, Any?>) -> RSP
    ): AsyncDBResponse<D, R, T, C, RSLT, RSP> {
        val arguments = query.bind(*values)
        return executor.preparedFetchAsync<D, R, T, Map<String, Any?>, Any, RSP>(query, HashMap, arguments.toList(), ctx, block)
    }
}

@Suppress("UNCHECKED_CAST")
class PreparedSelectFRM<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        val table: T,
        val sqlBuilder: ISQLBuilder<D, R, T>
) : IFRM<PreparedSelectQuery<D, R, T>> {
    override val logger = KotlinLogging.logger {}
    override var query: PreparedSelectQuery<D, R, T> =
            PreparedSelectQuery(sqlBuilder, SQLVerbs.SELECT, listOf(), listOf(), Table(table), listOf())

    suspend fun withColumns(vararg inColumns: Fields<*, D, *, *>): PreparedSelectFRMCustom<D, R, T> {
        query = query.copy(columns = inColumns.toList() as List<Fields<*, D, R, T>>)
        return PreparedSelectFRMCustom(query, table, sqlBuilder, logger)
    }

    suspend fun filter(block: suspend SQLConditionalBuilder<D, R, T>.() -> Unit): PreparedSelectFRM<D, R, T> {
        val state = SQLConditionalBuilder<D, R, T>()
        state.block()
        query = query.copy(conditions = state.conditions)
        return this
    }

    suspend fun <C : IExecutorConfig, RSLT, RSP> mapOne(
            executor: IAsyncExecutor<C, RSLT>,
            vararg values: Any,
            ctx: CoroutineContext = Dispatchers.Default,
            block: suspend (R) -> RSP
    ): AsyncDBResponse<D, R, T, C, RSLT, RSP> {
        val arguments = query.bind(*values)
        println(query.buildSQLString(executor.preparedPlaceHolder))
        return executor.preparedFetchAsync<D, R, T, R, Any, RSP>(query, SingleOfRecord, arguments.toList(), ctx, block)
    }

    suspend fun <C : IExecutorConfig, RSLT, RSP> map(
            executor: IAsyncExecutor<C, RSLT>,
            vararg values: Any,
            ctx: CoroutineContext = Dispatchers.Default,
            block: suspend (List<R>) -> RSP
    ): AsyncDBResponse<D, R, T, C, RSLT, RSP> {
        val arguments = query.bind(*values)
        return executor.preparedFetchAsync<D, R, T, List<R>, Any, RSP>(query, ListOfRecord, arguments.toList(), ctx, block)
    }
}

suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>
        lazyPreparedSelect(
        table: T,
        sqlBuilder: ISQLBuilder<D, R, T> = CommonSQLBuilder<D, R, T>()
): PreparedSelectFRM<D, R, T> =
        PreparedSelectFRM<D, R, T>(table, sqlBuilder)