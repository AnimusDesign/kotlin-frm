package design.animus.kotlin.frm.sql.query.common.frm

class UncommonInsertColumns(message: String) : Exception(message)