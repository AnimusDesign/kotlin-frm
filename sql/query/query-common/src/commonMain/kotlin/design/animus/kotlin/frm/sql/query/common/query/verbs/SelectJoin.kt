package design.animus.kotlin.frm.sql.query.common.query.verbs

import design.animus.kotlin.frm.sql.query.common.query.JoinType

interface ISelectJoinQuery : ISelectQuery {
    val joinStatements: List<JoinType>
}