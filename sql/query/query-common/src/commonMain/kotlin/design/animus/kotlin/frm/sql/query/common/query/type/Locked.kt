@file:Suppress("UNCHECKED_CAST")

package design.animus.kotlin.frm.sql.query.common.query.type

import design.animus.functional.datatypes.option.Option
import design.animus.kotlin.frm.sql.executors.common.logger
import design.animus.kotlin.frm.sql.query.common.builder.ISQLBuilder
import design.animus.kotlin.frm.sql.query.common.frm.InsertPair
import design.animus.kotlin.frm.sql.query.common.query.*
import design.animus.kotlin.frm.sql.query.common.query.verbs.*
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
import design.animus.kotlin.frm.sql.query.common.validateNestedQuery

sealed class LockedQuery<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> : IQuery<D, R, T> {
    override suspend fun buildSQLString() =
            when (this) {
                is SelectQuery -> sqlBuilder.buildSelectQuery(this).trim()
                is UpdateQuery -> sqlBuilder.buildUpdateQuery(this).trim()
                is InsertQuery -> sqlBuilder.buildInsertQuery(this).trim()
                is SelectJoinQuery -> sqlBuilder.buildSelectJoinQuery(this).trim()
                is DeleteQuery -> sqlBuilder.buildDeleteQuery(this)
            }.trim().replace("  ", " ")

    suspend fun <PR : ADatabaseRecord, PT : ITable<D, PR>> toField(): NestedQuery<*, D, PR, PT> {
        val possibleColumn = validateNestedQuery(this)
        return possibleColumn.foldAsync(
                {err ->
                    logger.error { err.message }
                    throw Exception(err.message)
                },
                {col ->
                    NestedQuery<Any?, D, PR, PT>(this as LockedQuery<D, PR, PT>, this.buildSQLString(), col.columnName, col.columnType, col.schema, col.table)
                }
        )
    }
}

data class SelectQuery<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        override val sqlBuilder: ISQLBuilder<D, R, T>,
        override val verb: SQLVerbs,
        override val columns: List<Fields<*, D, R, T>>,
        override val conditions: List<ConditionalSteps>,
        override val table: QueryTable,
        override val groupBy: List<Fields<*, *, *, *>>
) : ISelectQuery, LockedQuery<D, R, T>()

data class UpdateQuery<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        override val sqlBuilder: ISQLBuilder<D, R, T>,
        override val verb: SQLVerbs,
        override val conditions: List<ConditionalSteps>,
        override val table: QueryTable,
        override val changes: List<UpdateChange<*, D, R, T>>,
        override val returningFields: Option<List<Fields<Any?, D, R, T>>>
) : IUpdateQuery<D, R, T>, LockedQuery<D, R, T>()

data class InsertQuery<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        override val sqlBuilder: ISQLBuilder<D, R, T>,
        override val verb: SQLVerbs,
        override val table: QueryTable,
        override val newItems: List<InsertPair<D, R, T>>,
        override val returningFields: Option<List<Fields<Any?, D, R, T>>>
) : IInsertQuery<D, R, T>, LockedQuery<D, R, T>()

data class SelectJoinQuery<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        override val sqlBuilder: ISQLBuilder<D, R, T>,
        override val verb: SQLVerbs,
        override val columns: List<Fields<*, D, *, *>>,
        override val conditions: List<ConditionalSteps>,
        override val table: QueryTable,
        override val joinStatements: List<JoinType>,
        override val groupBy: List<Fields<*, *, *, *>>
) : ISelectJoinQuery, LockedQuery<D, R, T>()

data class DeleteQuery<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        override val sqlBuilder: ISQLBuilder<D, R, T>,
        override val table: QueryTable,
        override val conditions: List<ConditionalSteps>,
        override val verb: SQLVerbs = SQLVerbs.DELETE
) : IDeleteQuery<D, R, T>, LockedQuery<D, R, T>()