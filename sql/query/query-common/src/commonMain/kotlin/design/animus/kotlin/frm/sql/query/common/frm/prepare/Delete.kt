package design.animus.kotlin.frm.sql.query.common.frm.prepare


import design.animus.kotlin.frm.sql.executors.common.AsyncDBResponse
import design.animus.kotlin.frm.sql.executors.common.Delete
import design.animus.kotlin.frm.sql.executors.common.IAsyncExecutor
import design.animus.kotlin.frm.sql.executors.common.IExecutorConfig
import design.animus.kotlin.frm.sql.query.common.SQLConditionalBuilder
import design.animus.kotlin.frm.sql.query.common.builder.CommonSQLBuilder
import design.animus.kotlin.frm.sql.query.common.builder.ISQLBuilder
import design.animus.kotlin.frm.sql.query.common.frm.IFRM
import design.animus.kotlin.frm.sql.query.common.query.IDatabase
import design.animus.kotlin.frm.sql.query.common.query.ITable
import design.animus.kotlin.frm.sql.query.common.query.Table
import design.animus.kotlin.frm.sql.query.common.query.type.PreparedDeleteQuery
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
import kotlinx.coroutines.Dispatchers
import mu.KotlinLogging
import kotlin.coroutines.CoroutineContext

class PreparedDeleteFRM<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
    val table: T,
    val sqlBuilder: ISQLBuilder<D, R, T>
) : IFRM<PreparedDeleteQuery<D, R, T>> {
    override val logger = KotlinLogging.logger {}
    override var query = PreparedDeleteQuery<D, R, T>(sqlBuilder, Table(table))


    suspend fun deleteOn(condition: suspend  SQLConditionalBuilder<D,R,T>.() -> Unit): PreparedDeleteFRM<D, R, T> {
        val state = SQLConditionalBuilder<D,R,T>()
        state.condition()
        query = query.copy(conditions = state.conditions)
        return this
    }

    suspend fun <C : IExecutorConfig, RSLT> remove(
        asyncExecutor: IAsyncExecutor<C, RSLT>,
        vararg values: Any?,
        ctx:CoroutineContext=Dispatchers.Default
        ): AsyncDBResponse<D, R, T, C, RSLT, Boolean> {
        val arguments = query.bind(*values)
        return asyncExecutor.preparedFetchAsync<D, R, T, Boolean, Any, Boolean>(query, Delete, arguments, ctx) { x -> x }
    }
}

suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>
        lazyPreparedDelete(
    table: T,
    sqlBuilder: ISQLBuilder<D, R, T> = CommonSQLBuilder<D, R, T>()
): PreparedDeleteFRM<D, R, T> = PreparedDeleteFRM<D, R, T>(table, sqlBuilder)