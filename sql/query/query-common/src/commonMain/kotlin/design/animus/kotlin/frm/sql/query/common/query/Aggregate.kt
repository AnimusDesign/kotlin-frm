package design.animus.kotlin.frm.sql.query.common.query

import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord

interface ISQLAggregate {
    val functionName: String
}

sealed class CommonAggregates : ISQLAggregate

object Count : CommonAggregates() {
    override val functionName = "count"
}

object Sum : CommonAggregates() {
    override val functionName = "sum"
}

object Average : CommonAggregates() {
    override val functionName = "avg"
}

object Minimum : CommonAggregates() {
    override val functionName = "min"
}

object Maximum : CommonAggregates() {
    override val functionName = "max"
}

object Lower : CommonAggregates() {
    override val functionName = "lower"
}

internal suspend fun <C : Any?, D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, AG : ISQLAggregate> buildAggregateForSingleField(
    aggregate: AG, field: Fields<C, D, R, T>
) =
    AggregateField<C, D, R, T>(
        aggregate, field.columnName, field.columnType, field.schema, field.table, listOf(), false
    )

internal suspend fun <C : Any?, D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>, AG : ISQLAggregate> buildAggregateForFields(
    aggregate: AG, vararg fields: Fields<C, D, R, T>
) = AggregateField<C, D, R, T>(
    aggregate, "", "", "field.schema", "", fields.toList(), true
)


// Count
suspend fun <C : Any?, D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> count(field: Fields<C, D, R, T>) =
    buildAggregateForSingleField(Count, field)
// End Count

// Average
suspend fun <C : Any?, D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> avg(field: Fields<C, D, R, T>) =
    buildAggregateForSingleField(Average, field)
// End Average

// Sum
suspend fun <C : Any?, D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> sum(field: Fields<C, D, R, T>) =
    buildAggregateForSingleField(Sum, field)
// End Sum

// Minimum
suspend fun <C : Any?, D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> min(field: Fields<C, D, R, T>) =
    buildAggregateForSingleField(Minimum, field)
// End Minimum

// Maximum
suspend fun <C : Any?, D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> max(field: Fields<C, D, R, T>) =
    buildAggregateForSingleField(Maximum, field)
// End Minimum

// Maximum
suspend fun <C : Any?, D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> lower(field: Fields<C, D, R, T>): Fields<C, D, R, T> =
    buildAggregateForSingleField(Lower, field)
// End Minimum
