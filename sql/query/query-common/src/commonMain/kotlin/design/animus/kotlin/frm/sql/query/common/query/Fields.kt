package design.animus.kotlin.frm.sql.query.common.query

import design.animus.kotlin.frm.sql.query.common.builder.SQLQuery
import design.animus.kotlin.frm.sql.query.common.query.type.LockedQuery
import design.animus.kotlin.frm.sql.query.common.query.type.PreparedQuery
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord

data class PrimaryKeyColumn(
        val columnName: String,
        val columnType: String
)

sealed class Fields<C : Any?, D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        open val columnName: String,
        open val columnType: String,
        open val schema: String,
        open val table: String
)

abstract class Field<C : Any?, D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        override val columnName: String,
        override val columnType: String,
        override val schema: String,
        override val table: String
) : Fields<C, D, R, T>(columnName, columnType, schema, table) {
    override fun toString(): String {
        return "Field: ${table}.${columnName}"
    }

}

data class AggregateField<C : Any?, D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        val aggregate: ISQLAggregate,
        override val columnName: String,
        override val columnType: String,
        override val schema: String,
        override val table: String,
        val columns: List<Fields<*, D, *, *>> = listOf(),
        val multipleColumns: Boolean = false,
        val named: String = ""
) : Fields<C, D, R, T>(columnName, columnType, schema, table) {
    infix fun AS(value: String) = this.copy(named = value)
}

data class NamedField<C : Any?, D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        val field: Field<C, D, R, T>,
        val name: String,
        override val columnName: String = field.columnName,
        override val columnType: String = field.columnType,
        override val schema: String = field.schema,
        override val table: String = field.table
) : Fields<C, D, R, T>(columnName, columnType, schema, table)

abstract class PrimaryKey<C : Any?, D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        override val columnName: String,
        override val columnType: String,
        override val schema: String,
        override val table: String
) : Fields<C, D, R, T>(columnName, columnType, schema, table)

abstract class PrimaryKeys<C : Any?, D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        val columns: List<PrimaryKeyColumn>,
        override val columnName: String = columns.joinToString(",") { it.columnName },
        override val columnType: String = columns.joinToString(",") { it.columnType },
        override val table: String,
        override val schema: String
) : Fields<C, D, R, T>(columnName, columnType, schema, table)

abstract class LateField<C : Any?, D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        val simpleName: String,
        override val columnName: String = "",
        override val columnType: String = "",
        override val schema: String = "",
        override val table: String = ""
) : Fields<C, D, R, T>(columnName, columnType, schema, table) {
    override fun toString(): String {
        return "Lazy Field: ${this.table}.${this.columnName} expecting $simpleName"
    }
}

data class NestedQuery<C : Any?, D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        val query: LockedQuery<D, R, T>,
        val sql: SQLQuery,
        override val columnName: String,
        override val columnType: String,
        override val schema: String,
        override val table: String
) : Fields<C, D, R, T>(columnName, columnType, schema, table)

//data class NestedPreparedQuery<C : Any?, D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
//        val query: PreparedQuery<D, R, T, *>,
//        val sql: SQLQuery,
//        override val columnName: String,
//        override val columnType: String,
//        override val schema: String,
//        override val table: String
//) : Fields<C, D, R, T>(columnName, columnType, schema, table)

infix fun <C : Any?, D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> Field<C, D, R, T>.AS(value: String) =
        NamedField<C, D, R, T>(this, value)