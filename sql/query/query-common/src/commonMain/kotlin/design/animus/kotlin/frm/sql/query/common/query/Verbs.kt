package design.animus.kotlin.frm.sql.query.common.query

enum class SQLVerbs {
    SELECT,
    SELECTJOIN,
    UPDATE,
    INSERT,
    DELETE
}
