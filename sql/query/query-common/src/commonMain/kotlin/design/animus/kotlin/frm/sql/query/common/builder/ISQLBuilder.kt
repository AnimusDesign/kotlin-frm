package design.animus.kotlin.frm.sql.query.common.builder

import design.animus.functional.datatypes.option.Option
import design.animus.kotlin.frm.sql.executors.common.exceptions.MisMatchColumnSetForInsert
import design.animus.kotlin.frm.sql.executors.common.exceptions.NoValuesToAdd
import design.animus.kotlin.frm.sql.query.common.query.*
import design.animus.kotlin.frm.sql.query.common.query.type.NumberedCharacter
import design.animus.kotlin.frm.sql.query.common.query.type.PreparedPlacedHolder
import design.animus.kotlin.frm.sql.query.common.query.type.SimpleCharacter
import design.animus.kotlin.frm.sql.query.common.query.verbs.*
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
import mu.KLogger
import mu.KotlinLogging

typealias SQLQuery = String

const val magicPreparedKey =
        "IAMTHEBANANAHAMMOCKMANPLEASEREPLACEME0sSQYcZejtyTOqtsNZn6eLGwWFSqgKgSVobbiHzoWeZAtm95w95OCgPeRzo9yN5I"

interface ISQLBuilder<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> {
    val logger: KLogger

    fun getTablePair(queryTable: QueryTable): TableNamePair
    fun buildColumnString(schemaTable: SchemaTableName, friendlyTable: FriendlyTableName, field: Fields<*, *, *, *>)
            : String

    fun buildGroupBy(
            schemaTable: SchemaTableName,
            friendlyTable: FriendlyTableName,
            fields: List<Fields<*, *, *, *>>
    ): String

    fun escapeValue(value: Any?): String

    suspend fun createReturnStatement(returning: Option<List<Fields<Any?, D, R, T>>>) = Option.comprehension("") {
        val (fields) = returning
        when {
            fields.isEmpty() -> "returning *"
            else -> "returning ${fields.joinToString(",") { "${it.table}.${it.columnName}" }}"
        }
    }

    suspend fun buildWhereString(conditions: List<ConditionalSteps>, table: FriendlyTableName): WhereString
    suspend fun buildSelectQuery(query: ISelectQuery): SQLQuery
    suspend fun buildUpdateQuery(query: IUpdateQuery<D, R, T>): SQLQuery
    suspend fun buildInsertQuery(query: IInsertQuery<D, R, T>): SQLQuery
    suspend fun buildSelectJoinQuery(query: ISelectJoinQuery): SQLQuery
    suspend fun buildDeleteQuery(query: IDeleteQuery<D, R, T>): SQLQuery

    fun substitutePreparedVarWithExecutorVar(query: SQLQuery, variable: PreparedPlacedHolder): SQLQuery
}

class CommonSQLBuilder<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>> : ISQLBuilder<D, R, T> {
    override val logger = KotlinLogging.logger {}
    override fun getTablePair(queryTable: QueryTable): TableNamePair =
            when (queryTable) {
                is Table<*, *, *> -> {
                    val table = queryTable.table
                    Pair(
                            table.reflectedTableName,
                            table.reflectedTableName
                    )
                }
                is NamedTable<*, *, *> -> {
                    val namedTable = queryTable
                    Pair(
                            "${namedTable.table.reflectedTableName} as ${namedTable.name}",
                            namedTable.name
                    )
                }
            }

    override fun escapeValue(value: Any?): String = when (value) {
        Int, Long, Double, Float -> "$value"
        null -> "NULL"
        else -> "'$value'"
    }

    override fun buildColumnString(
            schemaTable: SchemaTableName,
            friendlyTable: FriendlyTableName,
            field: Fields<*, *, *, *>
    ) = when (field) {
        is PrimaryKey -> "${field.table}.${field.columnName}"
        is Field -> "${field.table}.${field.columnName}"
        is NamedField -> "${field.table}.${field.field.columnName} as ${field.name}"
        is PrimaryKeys -> field.columns.joinToString(",") {
            "${field.table}.${it.columnName}"
        }
        is AggregateField -> when (field.multipleColumns) {
            false -> "${field.aggregate.functionName}(${field.table}.${field.columnName})"
            true -> "${field.aggregate.functionName}(${field.columns.joinToString(",") { col ->
                "${field.table}.${col.columnName}"
            }})"
        }
        is LateField -> {
            logger.warn { "Late Fields are not supported in column selection." }
            ""
        }
        is NestedQuery<*, *, *, *> -> {
            "(${field.sql})"
        }
    }


    override fun buildGroupBy(
            schemaTable: SchemaTableName,
            friendlyTable: FriendlyTableName,
            fields: List<Fields<*, *, *, *>>
    ) = fields.joinToString(",") {
        buildColumnString(schemaTable, friendlyTable, it)
    }

    override suspend fun buildWhereString(conditions: List<ConditionalSteps>, table: FriendlyTableName) =
            commonBuildWhereString<D, R, T>(conditions, table).trim()

    override suspend fun buildSelectQuery(query: ISelectQuery): String {
        val (schemaTable, friendlyTable) = getTablePair(query.table)
        val columns = if (query.columns.isEmpty()) "*" else query.columns.joinToString(",") {
            buildColumnString(schemaTable, friendlyTable, it)
        }
        val where = if (query.conditions.isNotEmpty()) buildWhereString(query.conditions, friendlyTable) else ""
        return if (query.groupBy.isEmpty()) {
            "${query.verb.name} $columns from $schemaTable $where"
        } else {
            val groupBy = buildGroupBy(schemaTable, friendlyTable, query.groupBy)
            "${query.verb.name} $columns from $schemaTable $where group by $groupBy"
        }.trimIndent()
    }

    override suspend fun buildUpdateQuery(query: IUpdateQuery<D, R, T>): String {
        val (schemaTable, friendlyTable) = getTablePair(query.table)
        val where = buildWhereString(query.conditions, friendlyTable)
        val returning = createReturnStatement(query.returningFields)
        val changes = query.changes.joinToString(",") { change ->
            when (change.lateField) {
                null -> {
                    val value = escapeValue(change.value)
                    "$friendlyTable.${change.field.columnName} = $value"
                }
                else -> {
                    "$friendlyTable.${change.field.columnName} = $magicPreparedKey"
                }
            }
        }
        return "update $schemaTable SET $changes $where $returning".trimIndent()
    }

    override suspend fun buildInsertQuery(query: IInsertQuery<D, R, T>): String {
        val (schemaTable, friendlyTable) = getTablePair(query.table)
        val columns = query.newItems.map { it.map { f -> f.first } }
        if (columns.toSet().size != 1) throw MisMatchColumnSetForInsert("Was anticipating only 1 set of columns but received ${columns.toSet().size}")
        val columnNames = columns.first().joinToString(",") { "${it.table}.${it.columnName}" }
        val values = when {
            query.newItems.size > 1 -> {
                query.newItems.joinToString(",") { item ->
                    val a = item.joinToString(",") { escapeValue(it.second) }
                    "($a)"
                }
            }
            query.newItems.size == 1 -> {
                val items = query.newItems.first().map { escapeValue(it.second) }
                "(${items.joinToString(",")})"
            }
            else -> throw NoValuesToAdd("Expected values to be added but no values were provided.")
        }
        val returning = createReturnStatement(query.returningFields)
        return "${query.verb.name} into $schemaTable ($columnNames) values $values $returning".trimIndent()
    }

    override suspend fun buildSelectJoinQuery(query: ISelectJoinQuery): String {
        val (schemaTable, friendlyTable) = getTablePair(query.table)
        val columns = if (query.columns.isEmpty()) "*" else query.columns.joinToString(",") {
            buildColumnString(schemaTable, friendlyTable, it)
        }
        val joins = query.joinStatements.joinToString(" ") {
            val (joinSchemaTable, joinFriendlyTable) = getTablePair(it.table)
            when (it) {
                is Join -> "join $joinSchemaTable on ${buildConditionString(
                        it.condition,
                        joinFriendlyTable,
                        friendlyTable
                )}"
                is InnerJoin -> "inner join $joinSchemaTable on ${buildConditionString(
                        it.condition,
                        joinFriendlyTable,
                        friendlyTable
                )}"
                is RightJoin -> "right join $joinSchemaTable on ${buildConditionString(
                        it.condition,
                        joinFriendlyTable,
                        friendlyTable
                )}"
                is LeftJoin -> "left join $joinSchemaTable on ${buildConditionString(
                        it.condition,
                        joinFriendlyTable,
                        friendlyTable
                )}"
            }
        }
        val where = buildWhereString(query.conditions, friendlyTable)
        return """
        SELECT $columns from $schemaTable $joins $where
    """.trimIndent()
    }

    override suspend fun buildDeleteQuery(query: IDeleteQuery<D, R, T>): String {
        val (schemaTable, friendlyTable) = getTablePair(query.table)
        val where = buildWhereString(query.conditions, friendlyTable)
        return """
            ${query.verb.name} from $schemaTable $where
        """.trimIndent()
    }

    override fun substitutePreparedVarWithExecutorVar(
            query: SQLQuery,
            variable: PreparedPlacedHolder
    ): SQLQuery {
        return when (variable) {
            is SimpleCharacter -> {
                query.replace(magicPreparedKey, variable.preparedCharacter)
            }
            is NumberedCharacter -> {
                var idx = variable.startAt
                var newQuery = query
                while (newQuery.contains(magicPreparedKey)) {
                    newQuery = newQuery.replaceFirst(
                            magicPreparedKey,
                            if (variable.escapeChar != null) """${variable.escapeChar}${variable.preparedCharacter}$idx""" else "${variable.preparedCharacter}$idx"
                    )
                    idx += 1
                }
                newQuery
            }
        }.replace("'", "")
    }
}
