package design.animus.kotlin.frm.sql.query.common.frm

import design.animus.kotlin.frm.sql.executors.common.*
import design.animus.kotlin.frm.sql.query.common.SQLConditionalBuilder
import design.animus.kotlin.frm.sql.query.common.builder.CommonSQLBuilder
import design.animus.kotlin.frm.sql.query.common.builder.ISQLBuilder
import design.animus.kotlin.frm.sql.query.common.query.Fields
import design.animus.kotlin.frm.sql.query.common.query.IDatabase
import design.animus.kotlin.frm.sql.query.common.query.ITable
import design.animus.kotlin.frm.sql.query.common.query.Table
import design.animus.kotlin.frm.sql.query.common.query.type.SelectMutableQuery
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
import kotlinx.coroutines.Dispatchers
import mu.KLogger
import mu.KotlinLogging
import kotlin.coroutines.CoroutineContext


class SelectFRMCustom<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        var query: SelectMutableQuery<D, R, T>,
        val table: T,
        val sqlBuilder: ISQLBuilder<D, R, T>,
        val logger: KLogger = KotlinLogging.logger {}
) {
    suspend fun filter(block: suspend SQLConditionalBuilder<D, R, T>.() -> Unit): SelectFRMCustom<D, R, T> {
        val table = this
        val state = SQLConditionalBuilder<D, R, T>()
        state.block()
        query = query.copy(conditions = state.conditions).apply { this.table = Table(this@SelectFRMCustom.table) }
        return this
    }

    suspend fun <C : IExecutorConfig, RSLT, RSP> mapOne(
            executor: IAsyncExecutor<C, RSLT>,
            ctx: CoroutineContext = Dispatchers.Default,
            block: suspend (Map<String, Any?>) -> RSP
    ): AsyncDBResponse<D, R, T, C, RSLT, RSP> =
            executor.fetchAsync<D, R, T, Map<String, Any?>, Any, RSP>(query.lockQuery(sqlBuilder), HashMap, ctx, block)

}

class SelectFRM<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(
        val table: T,
        val sqlBuilder: ISQLBuilder<D, R, T>
) : IFRM<SelectMutableQuery<D, R, T>> {
    override val logger = KotlinLogging.logger {}
    override var query: SelectMutableQuery<D, R, T> =
            SelectMutableQuery<D, R, T>(listOf(), listOf(), listOf()).apply {
                table = Table(this@SelectFRM.table)
            }

    @Suppress("UNCHECKED_CAST")
    suspend fun withColumns(vararg inColumns: Fields<*, D, *, *>): SelectFRMCustom<D, R, T> {
        query = query.copy(columns = inColumns.toList() as List<Fields<*, D, R, T>>)
                .apply { table = Table(this@SelectFRM.table) }
        return SelectFRMCustom(query, table, sqlBuilder, logger)
    }

    @Suppress("UNCHECKED_CAST")
    suspend fun groupBy(vararg inColumns: Fields<*, D, *, *>): SelectFRM<D, R, T> {
        query = query.copy(groupBy = inColumns.toList() as List<Fields<*, D, R, T>>)
                .apply { table = Table(this@SelectFRM.table) }
        return this
    }

    suspend fun filter(block: suspend SQLConditionalBuilder<D, R, T>.() -> Unit): SelectFRM<D, R, T> {
        val table = this
        val state = SQLConditionalBuilder<D, R, T>()
        state.block()
        query = query.copy(conditions = state.conditions).apply { this.table = Table(this@SelectFRM.table) }
        return this
    }

    suspend fun <C : IExecutorConfig, RSLT, RSP> mapOne(
            executor: IAsyncExecutor<C, RSLT>,
            ctx: CoroutineContext = Dispatchers.Default,
            block: suspend (R) -> RSP
    ): AsyncDBResponse<D, R, T, C, RSLT, RSP> =
            executor.fetchAsync<D, R, T, R, Any, RSP>(query.lockQuery(sqlBuilder), SingleOfRecord, ctx, block)


    suspend fun <C : IExecutorConfig, RSLT, RSP> map(
            executor: IAsyncExecutor<C, RSLT>,
            ctx: CoroutineContext = Dispatchers.Default,
            block: suspend (List<R>) -> RSP
    ): AsyncDBResponse<D, R, T, C, RSLT, RSP> =
            executor.fetchAsync<D, R, T, List<R>, Any, RSP>(query.lockQuery(sqlBuilder), ListOfRecord, ctx, block)

    suspend fun <C : IExecutorConfig, RSLT> defer(
            executor: IAsyncExecutor<C, RSLT>, ctx: CoroutineContext = Dispatchers.Default
    ): AsyncDBResponse<D, R, T, C, RSLT, R> {
        val block: suspend (R) -> R = { x: R -> x }
        return executor.fetchAsync<D, R, T, R, Any, R>(
                query.lockQuery(sqlBuilder), DirectOfRecord, ctx, block
        )
    }

    suspend fun <C : IExecutorConfig, RSLT> deferMultiple(
            executor: IAsyncExecutor<C, RSLT>, ctx: CoroutineContext = Dispatchers.Default
    ) = executor.fetchAsync<D, R, T, List<R>, Any, List<R>>(
            query.lockQuery(sqlBuilder), DirectListOfRecord, ctx
    ) { x: List<R> -> x }
}

suspend fun <D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>
        lazySelect(
        table: T,
        sqlBuilder: ISQLBuilder<D, R, T> = CommonSQLBuilder<D, R, T>()
): SelectFRM<D, R, T> = SelectFRM<D, R, T>(table, sqlBuilder)