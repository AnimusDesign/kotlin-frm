package design.animus.kotlin.frm.sql.executors.common.errors

interface IExecutorError {
    val message: String
}

sealed class ExecutorErrors

data class CouldNotConnect(val databaseURL: String, override val message: String) : IExecutorError, ExecutorErrors()
data class NotConnected(override val message: String) : IExecutorError, ExecutorErrors()
data class EmptyResultSet(override val message: String) : IExecutorError, ExecutorErrors()
data class TooManyResults(override val message: String) : IExecutorError, ExecutorErrors()
data class UnexpectedException(override val message: String, val error: Throwable) : IExecutorError, ExecutorErrors()
