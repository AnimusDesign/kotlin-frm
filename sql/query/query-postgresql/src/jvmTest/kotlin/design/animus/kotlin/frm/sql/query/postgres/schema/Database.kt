package design.animus.kotlin.frm.sql.query.postgres.schema

import design.animus.kotlin.frm.sql.query.postgres.query.IPostgresDatabase

object Example : IPostgresDatabase {
    override val database = "postgres"
    override val schema = "example"
}
