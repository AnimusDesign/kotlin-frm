package design.animus.kotlin.frm.sql.query.postgres.query

import design.animus.kotlin.frm.sql.query.common.query.ISQLAggregate
import design.animus.kotlin.frm.sql.query.postgres.query.aggregates.JsonAggPair

sealed class PostgresSQLAggregate : ISQLAggregate

data class JsonAggregate(val struct: List<JsonAggPair>) : PostgresSQLAggregate() {
    override val functionName = "json_agg"
}

data class JsonObject(val pairs: List<JsonAggPair>) : PostgresSQLAggregate() {
    override val functionName = "json_build_object"
}

