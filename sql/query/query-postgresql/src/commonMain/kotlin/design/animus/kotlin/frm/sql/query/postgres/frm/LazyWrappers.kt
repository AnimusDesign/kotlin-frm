package design.animus.kotlin.frm.sql.query.postgres.frm

import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
import design.animus.kotlin.frm.sql.query.postgres.builder.PostgresSQLBuilder
import design.animus.kotlin.frm.sql.query.postgres.query.IPostgresDatabase
import design.animus.kotlin.frm.sql.query.postgres.query.IPostgresTable

suspend fun <D : IPostgresDatabase, R : ADatabaseRecord, T : IPostgresTable<D, R>>
        lazySelect(table: T) = design.animus.kotlin.frm.sql.query.common.frm.lazySelect(
    table, PostgresSQLBuilder()
)

suspend fun <D : IPostgresDatabase, R : ADatabaseRecord, T : IPostgresTable<D, R>>
        lazyInsert(table: T) = design.animus.kotlin.frm.sql.query.common.frm.lazyInsert(
    table,
    PostgresSQLBuilder()
)

suspend fun <D : IPostgresDatabase, R : ADatabaseRecord, T : IPostgresTable<D, R>>
        lazyUpdate(table: T) = design.animus.kotlin.frm.sql.query.common.frm.lazyUpdate(
    table,
    PostgresSQLBuilder()
)

suspend fun <D : IPostgresDatabase, R : ADatabaseRecord, T : IPostgresTable<D, R>>
        lazySelectJoin(table: T) = design.animus.kotlin.frm.sql.query.common.frm.lazySelectJoin(
    table,
    PostgresSQLBuilder()
)

suspend fun <D : IPostgresDatabase, R : ADatabaseRecord, T : IPostgresTable<D, R>>
        lazyDelete(table: T) = design.animus.kotlin.frm.sql.query.common.frm.lazyDelete(
    table,
    PostgresSQLBuilder()
)