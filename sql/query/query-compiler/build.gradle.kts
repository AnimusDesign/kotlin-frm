import java.net.URI

plugins {
    kotlin("jvm")
    id("kotlinx-serialization")
    id("maven-publish")
}

val group = "${extra["baseGroup"]}.sql"
val version = extra["projectVersion"] as String
val artifactVersion = extra["projectVersion"] as String
val artifactUrl = if (version.endsWith("SNAPSHOT")) {
    extra["artifactSnapShotURL"].toString()
} else {
    extra["artifactURL"].toString()
}

buildscript {
    apply("$rootDir/versions.gradle.kts")
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

kotlin {
    sourceSets {
        main {
            kotlin.setSrcDirs(
                mutableListOf("main", "generated")
            )
            resources.srcDir("main/resources")
            dependencies {
                implementation(project(":sql:query:query-common:"))
                implementation(project(":sql:query:query-postgresql:"))
                implementation(project(":sql:schema:schema-common:"))
                implementation(project(":sql:schema:schema-postgresql:"))
                implementation("io.github.microutils:kotlin-logging:${extra["KotlinLoggingVersion"]}")
                implementation(kotlin("stdlib-jdk8"))
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${extra["CoroutineVersion"]}")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-jdk8:${extra["CoroutineVersion"]}")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime:${extra["SerializationVersion"]}")
                implementation("org.jetbrains.kotlin:kotlin-reflect:${extra["KotlinVersion"]}")
                implementation("ch.qos.logback:logback-core:${extra["LogBackVersion"]}")
                implementation("ch.qos.logback:logback-classic:${extra["LogBackVersion"]}")
                implementation("com.squareup:kotlinpoet:${extra["KotlinPoetVersion"]}")
            }
        }
        test {
            kotlin.srcDir("test")
            resources.srcDir("test/resources")
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
                implementation(kotlin("test-junit"))
                implementation(kotlin("test"))
                implementation(project(":sql:query:query-common:"))
                implementation(project(":sql:query:query-postgresql:"))
            }
        }
    }
}

publishing {
    repositories {
        maven {
            url = URI(artifactUrl)
            credentials {
                username = extra["artifactUser"] as String?
                password = extra["artifactPassword"] as String?
            }
        }
    }
    publications {
        create<MavenPublication>("maven") {
            this.groupId = group
            this.version = artifactVersion
            artifactId = "executor-jasync-postgres"
            from(components["java"])
        }
    }
}