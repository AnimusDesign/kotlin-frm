package design.animus.kotlin.frm.sql.query.compiler.records

import design.animus.kotlin.frm.sql.query.common.query.IDatabase
import design.animus.kotlin.frm.sql.query.common.query.ITable
import design.animus.kotlin.frm.sql.query.common.query.type.IBaseQuery
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord

sealed class QueryType<D : IDatabase, R : ADatabaseRecord, T : ITable<D, R>>(open val query: IBaseQuery<D, R, T>)