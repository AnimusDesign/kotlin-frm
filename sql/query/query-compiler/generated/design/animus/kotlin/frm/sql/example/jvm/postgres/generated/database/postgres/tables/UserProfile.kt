package design.animus.kotlin.frm.sql.example.jvm.postgres.generated.database.postgres.tables

import design.animus.kotlin.frm.sql.example.jvm.postgres.generated.database.postgres.Postgres
import design.animus.kotlin.frm.sql.query.common.query.Field
import design.animus.kotlin.frm.sql.query.common.query.LateField
import design.animus.kotlin.frm.sql.query.common.query.PrimaryKey
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecordByMap
import design.animus.kotlin.frm.sql.query.postgres.query.IPostgresTable
import kotlin.Any
import kotlin.Int
import kotlin.String
import kotlin.collections.List
import kotlin.collections.Map
import kotlin.reflect.KClass

object UserProfile : IPostgresTable<Postgres, UserProfileRecord> {
    val id: PrimaryKey<Int, Postgres, UserProfileRecord, UserProfile> = UserProfileId

    val email: Field<String?, Postgres, UserProfileRecord, UserProfile> = UserProfileEmail

    val bio: Field<String?, Postgres, UserProfileRecord, UserProfile> = UserProfileBio

    val lazyId: LateField<Int, Postgres, UserProfileRecord, UserProfile> = UserProfileLazyId

    val lazyEmail: LateField<String?, Postgres, UserProfileRecord, UserProfile> = UserProfileLazyEmail

    val lazyBio: LateField<String?, Postgres, UserProfileRecord, UserProfile> = UserProfileLazyBio

    override val reflectedTableName: String = "user_profile"

    override val reflectedDatabase: Postgres = Postgres

    override val reflectedColumns: List<String> = listOf("id", "email", "bio")

    override val reflectedRecordClass: KClass<UserProfileRecord> = UserProfileRecord::class

    override val reflectedTable: IPostgresTable<Postgres, UserProfileRecord> = this
}

class UserProfileRecord(
    private val map: Map<String, Any>
) : ADatabaseRecordByMap(map) {
    val id: Int by map

    val email: String? by map

    val bio: String? by map
}

object UserProfileId : PrimaryKey<Int, Postgres, UserProfileRecord, UserProfile>(
    "id", "integer",
    "jasync_example", "user_profile"
)

object UserProfileEmail : Field<String?, Postgres, UserProfileRecord, UserProfile>(
    "email",
    "character varying", "jasync_example", "user_profile"
)

object UserProfileBio : Field<String?, Postgres, UserProfileRecord, UserProfile>(
    "bio", "text",
    "jasync_example", "user_profile"
)

object UserProfileLazyId : LateField<Int, Postgres, UserProfileRecord, UserProfile>(
    "Int", "id",
    "integer", "jasync_example", "user_profile"
)

object UserProfileLazyEmail : LateField<String?, Postgres, UserProfileRecord, UserProfile>(
    "String",
    "email", "character varying", "jasync_example", "user_profile"
)

object UserProfileLazyBio : LateField<String?, Postgres, UserProfileRecord, UserProfile>(
    "String",
    "bio", "text", "jasync_example", "user_profile"
)
