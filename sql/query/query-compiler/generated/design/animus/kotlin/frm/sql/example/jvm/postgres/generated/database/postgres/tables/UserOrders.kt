package design.animus.kotlin.frm.sql.example.jvm.postgres.generated.database.postgres.tables

import design.animus.kotlin.frm.sql.example.jvm.postgres.generated.database.postgres.Postgres
import design.animus.kotlin.frm.sql.query.common.query.Field
import design.animus.kotlin.frm.sql.query.common.query.LateField
import design.animus.kotlin.frm.sql.query.common.query.PrimaryKey
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecordByMap
import design.animus.kotlin.frm.sql.query.postgres.query.IPostgresTable
import kotlin.Any
import kotlin.Int
import kotlin.String
import kotlin.collections.List
import kotlin.collections.Map
import kotlin.reflect.KClass

object UserOrders : IPostgresTable<Postgres, UserOrdersRecord> {
    val id: PrimaryKey<Int, Postgres, UserOrdersRecord, UserOrders> = UserOrdersId

    val userId: Field<Int, Postgres, UserOrdersRecord, UserOrders> = UserOrdersUserId

    val inventoryId: Field<Int, Postgres, UserOrdersRecord, UserOrders> = UserOrdersInventoryId

    val orderPlaced: Field<String, Postgres, UserOrdersRecord, UserOrders> = UserOrdersOrderPlaced

    val lazyId: LateField<Int, Postgres, UserOrdersRecord, UserOrders> = UserOrdersLazyId

    val lazyUserId: LateField<Int, Postgres, UserOrdersRecord, UserOrders> = UserOrdersLazyUserId

    val lazyInventoryId: LateField<Int, Postgres, UserOrdersRecord, UserOrders> =
        UserOrdersLazyInventoryId

    val lazyOrderPlaced: LateField<String, Postgres, UserOrdersRecord, UserOrders> =
        UserOrdersLazyOrderPlaced

    override val reflectedTableName: String = "user_orders"

    override val reflectedDatabase: Postgres = Postgres

    override val reflectedColumns: List<String> = listOf("id", "userId", "inventoryId", "orderPlaced")

    override val reflectedRecordClass: KClass<UserOrdersRecord> = UserOrdersRecord::class

    override val reflectedTable: IPostgresTable<Postgres, UserOrdersRecord> = this
}

class UserOrdersRecord(
    private val map: Map<String, Any>
) : ADatabaseRecordByMap(map) {
    val id: Int by map

    val userId: Int by map

    val inventoryId: Int by map

    val orderPlaced: String by map
}

object UserOrdersId : PrimaryKey<Int, Postgres, UserOrdersRecord, UserOrders>(
    "id", "integer",
    "jasync_example", "user_orders"
)

object UserOrdersUserId : Field<Int, Postgres, UserOrdersRecord, UserOrders>(
    "user_id", "integer",
    "jasync_example", "user_orders"
)

object UserOrdersInventoryId : Field<Int, Postgres, UserOrdersRecord, UserOrders>(
    "inventory_id",
    "integer", "jasync_example", "user_orders"
)

object UserOrdersOrderPlaced : Field<String, Postgres, UserOrdersRecord, UserOrders>(
    "order_placed",
    "timestamp without time zone", "jasync_example", "user_orders"
)

object UserOrdersLazyId : LateField<Int, Postgres, UserOrdersRecord, UserOrders>(
    "Int", "id",
    "integer", "jasync_example", "user_orders"
)

object UserOrdersLazyUserId : LateField<Int, Postgres, UserOrdersRecord, UserOrders>(
    "Int",
    "user_id", "integer", "jasync_example", "user_orders"
)

object UserOrdersLazyInventoryId : LateField<Int, Postgres, UserOrdersRecord, UserOrders>(
    "Int",
    "inventory_id", "integer", "jasync_example", "user_orders"
)

object UserOrdersLazyOrderPlaced : LateField<String, Postgres, UserOrdersRecord,
        UserOrders>(
    "String", "order_placed", "timestamp without time zone", "jasync_example",
    "user_orders"
)
