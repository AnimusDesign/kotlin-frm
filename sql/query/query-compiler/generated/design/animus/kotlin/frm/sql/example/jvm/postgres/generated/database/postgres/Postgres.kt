package design.animus.kotlin.frm.sql.example.jvm.postgres.generated.database.postgres

import design.animus.kotlin.frm.sql.query.postgres.query.IPostgresDatabase
import kotlin.String

object Postgres : IPostgresDatabase {
    override val database: String = "postgres"

    override val schema: String = "jasync_example"
}
