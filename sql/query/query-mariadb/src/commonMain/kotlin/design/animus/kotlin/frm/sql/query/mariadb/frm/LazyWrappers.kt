package design.animus.kotlin.frm.sql.query.mariadb.frm

import design.animus.kotlin.frm.sql.query.common.builder.CommonSQLBuilder
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord
import design.animus.kotlin.frm.sql.query.mariadb.query.IMariaDB
import design.animus.kotlin.frm.sql.query.mariadb.query.IMariaDBTable

suspend fun <D : IMariaDB, R : ADatabaseRecord, T : IMariaDBTable<D, R>>
        lazySelect(table: T) = design.animus.kotlin.frm.sql.query.common.frm.lazySelect(
    table, CommonSQLBuilder()
)

suspend fun <D : IMariaDB, R : ADatabaseRecord, T : IMariaDBTable<D, R>>
        lazyInsert(table: T) = design.animus.kotlin.frm.sql.query.common.frm.lazyInsert(
    table,
    CommonSQLBuilder()
)

suspend fun <D : IMariaDB, R : ADatabaseRecord, T : IMariaDBTable<D, R>>
        lazyUpdate(table: T) = design.animus.kotlin.frm.sql.query.common.frm.lazyUpdate(
    table,
    CommonSQLBuilder()
)

suspend fun <D : IMariaDB, R : ADatabaseRecord, T : IMariaDBTable<D, R>>
        lazySelectJoin(table: T) = design.animus.kotlin.frm.sql.query.common.frm.lazySelectJoin(
    table,
    CommonSQLBuilder()
)

suspend fun <D : IMariaDB, R : ADatabaseRecord, T : IMariaDBTable<D, R>>
        lazyDelete(table: T) = design.animus.kotlin.frm.sql.query.common.frm.lazyDelete(
    table,
    CommonSQLBuilder()
)