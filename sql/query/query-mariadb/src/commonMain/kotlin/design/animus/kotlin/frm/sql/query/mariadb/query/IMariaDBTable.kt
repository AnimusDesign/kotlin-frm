package design.animus.kotlin.frm.sql.query.mariadb.query

import design.animus.kotlin.frm.sql.query.common.query.ITable
import design.animus.kotlin.frm.sql.query.common.response.ADatabaseRecord

interface IMariaDBTable<D : IMariaDB, R : ADatabaseRecord> : ITable<D, R>