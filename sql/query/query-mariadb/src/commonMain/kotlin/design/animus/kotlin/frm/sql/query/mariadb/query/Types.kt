package design.animus.kotlin.frm.sql.query.mariadb.query

import design.animus.kotlin.frm.sql.query.common.ClassName
import design.animus.kotlin.frm.sql.query.common.ISQLType

sealed class MariaDBTypes : ISQLType()

object Date : MariaDBTypes() {
    override val sqlType = "date"
    override val jvmType = ClassName("java.time", "LocalDate")
    override val nativeType = ClassName("kotlin", "String")
    override val jsType = ClassName("kotlin", "String")
    override val commonType = ClassName("kotlin", "String")
}

object DateTime : MariaDBTypes() {
    override val sqlType = "datetime"
    override val jvmType = ClassName("java.time", "LocalDateTime")
    override val nativeType = ClassName("kotlin", "String")
    override val jsType = ClassName("kotlin", "String")
    override val commonType = ClassName("kotlin", "String")
}

object LocalDate : MariaDBTypes() {
    override val sqlType = "new_date"
    override val jvmType = ClassName("java.time", "LocalDate")
    override val nativeType = ClassName("kotlin", "String")
    override val jsType = ClassName("kotlin", "String")
    override val commonType = ClassName("kotlin", "String")
}

object TimeStamp : MariaDBTypes() {
    override val sqlType = "timestamp"
    override val jvmType = ClassName("java.time", "LocalDateTime")
    override val nativeType = ClassName("kotlin", "Long")
    override val jsType = ClassName("kotlin", "Long")
    override val commonType = ClassName("kotlin", "Long")

}

object MariaDBByte : MariaDBTypes() {
    override val sqlType = "tinyint"
    override val jvmType = ClassName("kotlin", "Byte")
    override val nativeType = ClassName("kotlin", "Byte")
    override val jsType = ClassName("kotlin", "Byte")
    override val commonType = ClassName("kotlin", "Byte")
}

object SmallInt : MariaDBTypes() {
    override val sqlType = "smallint"
    override val jvmType = ClassName("kotlin", "Short")
    override val nativeType = ClassName("kotlin", "Short")
    override val jsType = ClassName("kotlin", "Short")
    override val commonType = ClassName("kotlin", "Short")
}

object Year : MariaDBTypes() {
    override val sqlType = "year"
    override val jvmType = ClassName("kotlin", "Short")
    override val nativeType = ClassName("kotlin", "Short")
    override val jsType = ClassName("kotlin", "Short")
    override val commonType = ClassName("kotlin", "Short")
}

object MariaDBFloat : MariaDBTypes() {
    override val sqlType = "float"
    override val jvmType = ClassName("kotlin", "Float")
    override val nativeType = ClassName("kotlin", "Float")
    override val jsType = ClassName("kotlin", "Float")
    override val commonType = ClassName("kotlin", "Float")
}

object MariaDBDouble : MariaDBTypes() {
    override val sqlType = "double"
    override val jvmType = ClassName("kotlin", "Double")
    override val nativeType = ClassName("kotlin", "Double")
    override val jsType = ClassName("kotlin", "Double")
    override val commonType = ClassName("kotlin", "Double")
}

object Int24 : MariaDBTypes() {
    override val sqlType = "int24"
    override val jvmType = ClassName("kotlin", "Int")
    override val nativeType = ClassName("kotlin", "Int")
    override val jsType = ClassName("kotlin", "Int")
    override val commonType = ClassName("kotlin", "Int")
}

object MariaDBInt : MariaDBTypes() {
    override val sqlType = "int"
    override val jvmType = ClassName("kotlin", "Int")
    override val nativeType = ClassName("kotlin", "Int")
    override val jsType = ClassName("kotlin", "Int")
    override val commonType = ClassName("kotlin", "Int")
}

object MediumInt : MariaDBTypes() {
    override val sqlType = "mediumint"
    override val jvmType = ClassName("kotlin", "Int")
    override val nativeType = ClassName("kotlin", "Int")
    override val jsType = ClassName("kotlin", "Int")
    override val commonType = ClassName("kotlin", "Int")
}

object BigInt : MariaDBTypes() {
    override val sqlType = "bigint"
    override val jvmType = ClassName("kotlin", "Long")
    override val nativeType = ClassName("kotlin", "Long")
    override val jsType = ClassName("kotlin", "Long")
    override val commonType = ClassName("kotlin", "Long")
}

object Numeric : MariaDBTypes() {
    override val sqlType = "numeric"
    override val jvmType = ClassName("java.math", "BigDecimal")
    override val nativeType = ClassName("kotlin", "Number")
    override val jsType = ClassName("kotlin", "Number")
    override val commonType = ClassName("kotlin", "Number")
}

object NewDecimal : MariaDBTypes() {
    override val sqlType = "new_decimal"
    override val jvmType = ClassName("java.math", "BigDecimal")
    override val nativeType = ClassName("kotlin", "String")
    override val jsType = ClassName("kotlin", "String")
    override val commonType = ClassName("kotlin", "String")
}

object MariaDBDecimal : MariaDBTypes() {
    override val sqlType = "decimal"
    override val jvmType = ClassName("java.math", "BigDecimal")
    override val nativeType = ClassName("kotlin", "String")
    override val jsType = ClassName("kotlin", "String")
    override val commonType = ClassName("kotlin", "String")
}

object MariaDBString : MariaDBTypes() {
    override val sqlType = "string"
    override val jvmType = ClassName("kotlin", "String")
    override val nativeType = ClassName("kotlin", "String")
    override val jsType = ClassName("kotlin", "String")
    override val commonType = ClassName("kotlin", "String")
}

object VarString : MariaDBTypes() {
    override val sqlType = "var_string"
    override val jvmType = ClassName("kotlin", "String")
    override val nativeType = ClassName("kotlin", "String")
    override val jsType = ClassName("kotlin", "String")
    override val commonType = ClassName("kotlin", "String")
}

object VarChar : MariaDBTypes() {
    override val sqlType = "varchar"
    override val jvmType = ClassName("kotlin", "String")
    override val nativeType = ClassName("kotlin", "String")
    override val jsType = ClassName("kotlin", "String")
    override val commonType = ClassName("kotlin", "String")
}

object Time : MariaDBTypes() {
    override val sqlType = "time"
    override val jvmType = ClassName("java.util", "Duration")
    override val nativeType = ClassName("kotlin", "String")
    override val jsType = ClassName("kotlin", "String")
    override val commonType = ClassName("kotlin", "String")
}

object Text : MariaDBTypes() {
    override val sqlType = "text"
    override val jvmType = ClassName("kotlin", "String")
    override val nativeType = ClassName("kotlin", "String")
    override val jsType = ClassName("kotlin", "String")
    override val commonType = ClassName("kotlin", "String")
}


object JSON : MariaDBTypes() {
    override val sqlType = "JSON"
    override val jvmType = ClassName("kotlin", "String")
    override val nativeType = ClassName("kotlin", "String")
    override val jsType = ClassName("kotlin", "String")
    override val commonType = ClassName("kotlin", "String")
}

object ENUM : MariaDBTypes() {
    override val sqlType = "enum"
    override val jvmType = ClassName("kotlin", "String")
    override val nativeType = ClassName("kotlin", "String")
    override val jsType = ClassName("kotlin", "String")
    override val commonType = ClassName("kotlin", "String")
}

object Blob : MariaDBTypes() {
    override val sqlType = "blob"
    override val jvmType = ClassName("kotlin", "ByteArray")
    override val nativeType = ClassName("kotlin", "ByteArray")
    override val jsType = ClassName("kotlin", "ByteArray")
    override val commonType = ClassName("kotlin", "ByteArray")
}

