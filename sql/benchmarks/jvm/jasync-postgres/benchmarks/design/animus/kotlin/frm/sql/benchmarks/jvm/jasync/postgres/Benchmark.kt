package design.animus.kotlin.frm.sql.benchmarks.jvm.jasync.postgres

import com.github.jasync.sql.db.QueryResult
import design.animus.kotlin.frm.sql.example.jvm.postgres.generated.database.postgres.Postgres
import design.animus.kotlin.frm.sql.example.jvm.postgres.generated.database.postgres.tables.Users
import design.animus.kotlin.frm.sql.example.jvm.postgres.generated.database.postgres.tables.UsersRecord
import design.animus.kotlin.frm.sql.executors.jvm.jasync.JaSyncPostgresConfig
import design.animus.kotlin.frm.sql.executors.jvm.jasync.JaSyncPostgresExecutor
import design.animus.kotlin.frm.sql.query.common.frm.filter
import design.animus.kotlin.frm.sql.query.postgres.query.frm.map
import kotlinx.coroutines.runBlocking
import org.openjdk.jmh.annotations.*
import java.util.concurrent.CompletableFuture
import java.util.concurrent.TimeUnit

@State(Scope.Benchmark)
@Fork(1)
@Warmup(iterations = 0)
@Measurement(iterations = 1, time = 1, timeUnit = TimeUnit.SECONDS)
class JasyncBenchmark {
    private var data = 0.0
    lateinit var executor: JaSyncPostgresExecutor
    private val benchMarkConfig = JaSyncPostgresConfig(
        System.getenv("pgDataBaseHost"),
        System.getenv("pgDataBase"),
        System.getenv("pgDataBaseUser"),
        System.getenv("pgDataBasePassword"),
        5432,
        "example"
    )

    @Setup
    fun setUp() = runBlocking {
        executor = JaSyncPostgresExecutor()
        executor.connect(benchMarkConfig)
    }

    @Benchmark
    fun selectSpecificUser() = runBlocking {

        val result = Users
            .filter {
                (Users.firstName equal "jack")
            }
            .map<Postgres, UsersRecord, Users, JaSyncPostgresConfig, CompletableFuture<QueryResult>, List<UsersRecord>>(
                executor
            ) {
                it
            }
        result.fetch()
    }
}
