
#### Stable Versions

```
https://nexus.animus.design/repository/AnimusDesign/
```

#### Snapshot Versions

```
https://nexus.animus.design/repository/AnimusDesignSnapshot/
```