There is a common configuration shared across all the module types.
This assumes a base requisite of common criteria to connect to a data
base. 

In code it is defined as:

```kotlin
interface ISChemaGeneratorConfig {
    var dataBases: List<String>
    var dataBaseHost: String
    var dataBaseUser: String
    var dataBasePassword: String
    var dataBasePort: Int
    var namespace: String
    var outputDir: String
    var logLevel: String
    var platforms: Set<Platform>
    var excludeTables: Set<String>
}
```


This will lead to a base configuration of:

```kotlin
databaseGeneratorConfig {
    dataBases = listOf(db)
    dataBaseHost = dbHost
    dataBaseUser = dbUser
    dataBasePassword = dbPassword
    dataBasePort = dbPort
    namespace = "design.animus.kotlin.frm.sql.example.jvm.postgres.generated"
    outputDir = "$projectDir/generated"
    dataBaseSchema = "example"
    platforms = setOf(design.animus.kotlin.frm.sql.schema.common.JVM)
    excludeTables = setOf("flyway_schema_history")
}
```

* **dataBases** A list of databases to reflect on.
* **dataBaseHost** The database to connect too.
* **dataBaseUser** The data base user.
* **dataBasePassword** The data base user password.
* **dataBasePort** The data base port
* **namespace** The name space/package of the out putted code.
* **outputDir** Where the generated code will go.
* **dataBaseSchema** Which schema to pull from.
* **platforms** A set of Platform, noted below.
    * *JavaScript*
    * *JVM*
    * *Common*
    
* *Note: If you do more than one platform it will generate the multiple directory source set.*

A number of these are self explanatory. But the stand outs are where the codes go (`outputDir`),
and the target platforms.  

**!!!!!!!!!!!!!!!!!!!!!!!!!!!**

**Important**

**!!!!!!!!!!!!!!!!!!!!!!!!!!!**

[See the section on the gradle file](./sql/gradle/generated_gradle.md) for the build gradle of the generated code.