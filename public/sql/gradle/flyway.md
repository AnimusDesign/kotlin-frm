Flyway is a database migration and versioning tool. It maintains
a list of `sql` files, inside your resource directory. Applying
those changes to the database. Ensuring it is at the latest version.

Instead of your `kotlin` or `java` code. Driving the data base generation.
The `sql` specification drives the code generation. 

#### Environment Variables

For ease of use it is recommended to utilize environment variables
to drive the configuration. These should be stored as secrets in
your `CI/CD` system. 

**Connection Variables**

```groovy 
def dbHost = System.getenv("dataBaseHost")
def db = System.getenv("dataBase")
def dbPort = System.getenv("dataBasePort").toInteger()
def dbUser = System.getenv("dataBaseUser")
def dbPassword = System.getenv("dataBasePassword")
```
In the above snippet we are pulling the configuration from `environment variables`.
However there may be an instance where there are `null` variables. The primary
example would be use with Intellij. Using null coalescing will alleviate this, noted
in the below example.

```groovy
def dbHost = System.getenv("dataBaseHost") ?: "localhost"
def db = System.getenv("dataBase") ?: "postgres"
def dbPort = (System.getenv("dataBasePort") ?:"5432").toInteger()
def dbUser = System.getenv("dataBaseUser") ?: "postgres"
def dbPassword = System.getenv("dataBasePassword") ?: "postgres"
```

#### Flyway

**Plugin**

[Latest version can be found here](https://mvnrepository.com/artifact/org.flywaydb/flyway-core)

^ Don't blindly copy the below, grab the latest version, and change it!

```groovy
buildscript {
    ext {
        FlyWayDBVersion = "6.0.7"       
    }
}

plugins {
    id "org.flywaydb.flyway" version "$FlyWayDBVersion"
}
```              

#### Optional (*Adjust Source Set*)

I always move `kotlin` projects up from `src/main/kotlin` `->` `main`.
This will adjust the root source set levels. 

```groovy
sourceSets {
    main.resources.srcDirs += "main/resources"
    main.kotlin.srcDirs += "main"
    main.kotlin.srcDirs += "generated"
    test.kotlin.srcDirs += "test"
    test.resources.srcDirs += "test/resources"
}        
```                                                                   

#### Generating the Database code

* **Optional**
    * ./main/resources/db/migration
* **Default**
    * ./src/main/resources/db/migration
    
Create a file I usually start with `V1__InitDB.sql`, but name at your
preference.  
