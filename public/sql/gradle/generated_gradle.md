#### Groovy DSL

Below is a snippet for a multi platform generated library. 

**Note**

This just includes `query-common`, you will also want to add the specific package
for your given database, i.e. `query-postgresql`. 


```groovy
apply plugin: "maven-publish"
apply plugin: "org.jetbrains.kotlin.multiplatform"

kotlin {
    jvm()
    js {
        browser {
        }
        nodejs {
        }
    }
    sourceSets {
        commonMain {
            dependencies {
                implementation "org.jetbrains.kotlinx:kotlinx-coroutines-core:$CoroutineVersion"
                implementation "org.jetbrains.kotlinx:kotlinx-coroutines-core-common:$CoroutineVersion"
                implementation kotlin("stdlib-common")
                implementation(project(":sql:query:query-common:"))
            }
        }
        commonTest {
            dependencies {
                implementation "org.jetbrains.kotlinx:kotlinx-coroutines-core:$CoroutineVersion"
                implementation "org.jetbrains.kotlinx:kotlinx-coroutines-core-common:$CoroutineVersion"
                implementation kotlin("test-common")
                implementation kotlin("test-annotations-common")
            }
        }
        jvmMain {
            dependencies {
                implementation kotlin("stdlib-jdk8")
                implementation "org.jetbrains.kotlinx:kotlinx-coroutines-core:$CoroutineVersion"
                implementation(project(":sql:query:query-common:"))
            }
        }
        jvmTest {
            dependencies {
                implementation "org.jetbrains.kotlinx:kotlinx-coroutines-core:$CoroutineVersion"
                implementation kotlin("test")
                implementation kotlin("test-junit")
            }
        }
        jsMain {
            dependencies {
                implementation "org.jetbrains.kotlinx:kotlinx-coroutines-core:$CoroutineVersion"
                implementation "org.jetbrains.kotlinx:kotlinx-coroutines-core-js:$CoroutineVersion"
                implementation kotlin("stdlib-js")
                implementation(project(":sql:query:query-common:"))
            }
        }
        jsTest {
            dependencies {
                implementation "org.jetbrains.kotlinx:kotlinx-coroutines-core:$CoroutineVersion"
                implementation kotlin("test-js")
            }
        }
    }
}

publishing {
    repositories {
        maven {
            url = version.endsWith('SNAPSHOT') ? artifactSnapShotURL : artifactURL
            credentials {
                username = artifactUser
                password = artifactPassword
            }
        }
    }
    publications {
        kotlinMultiplatform {
            group = group
            version = version
            artifactId = "query-postgres"
        }
    }
}
```