All executors share a common flow. The difference is whether it's blocking or not.

#### Non Blocking

Non blocking calls are wrappepd in an `AsyncDBResponse`. These wrap the future type,
whether it be `CompletableFuture`, `Deferred`, etc. An example is probably best. 

```kotlin
 val frmConfig = JaSyncPostgresConfig(
        System.getenv("pgDataBaseHost"),
        System.getenv("pgDataBase"),
        System.getenv("pgDataBaseUser"),
        System.getenv("pgDataBasePassword"),
        5432, // Port
        "example" // Schema
)
val executor = JaSyncPostgresExecutor()
executor.connect(FRMTestConfig)
val delayedResult = Users
        .filter {
            (Users.firstName equal "jack") and (Users.lastName equal "smith")
        } // Up to this point a SelectQuery was Built
        .mapOne(executor) { // A connection is pulled from the JaSync pool
            "Hello ${it.firstName} ${it.lastName}" // This is returned 
        }
// other things happen here
val welcomeMessage = delayedResult.fetch()
```

1. We build the query.
2. We pass it into an executor, with an anonymous function
    * **The anonymous function is not executed immediately**
3. An `AsyncDBResponse` is returned.
    * The `AsyncDBResponse` contains a deferred response.
        * I.E. in `JaSync` it is `CompletableFuture<QueryResult>`
4. We run fetch.
    * We `await` the deferred data base result.
    * We map the `CompletableFuture<QueryResult>` -> the data class `Record`
        * This is delegated by map to avoid the cost of reflection
    * The `Record` is then passed into the closure
    * The closure executes returning the final result.
    
**I just want a data class of my data.**

```kotlin
 val frmConfig = JaSyncPostgresConfig(
        System.getenv("pgDataBaseHost"),
        System.getenv("pgDataBase"),
        System.getenv("pgDataBaseUser"),
        System.getenv("pgDataBasePassword"),
        5432, // Port
        "example" // Schema
)
val executor = JaSyncPostgresExecutor()
executor.connect(FRMTestConfig)
val delayedResult = Users
        .filter {
            (Users.firstName equal "jack") and (Users.lastName equal "smith")
        } // Up to this point a SelectQuery was Built
        .defer()
val result = delayedResult.fetch()
```

It is very similar to the above. But we just return the result (*Record*).