An executor takes in a `compiled/constructed` query. Passing it into an executor. The executor
can be `jdbc`, `ja-sync`, `node-postgres`, etc. Executors adhere to an interface. With multiple
executor interfaces being defined.

#### Contributing

Building an executor is relatively simple. There are a number of pre-built ones noted
below. But we always appreciate more being added.

#### JVM

##### Postgres

* [**JaSync Postgres**](./sql/executors/jvm/postgres/jasync.md) - A non blocking postgres driver built atop of netty.
* [**Vertx Postgres**](./sql/executors/jvm/postgres/vertx.md) - A non blocking postgres driver bound to a vertx instance.

