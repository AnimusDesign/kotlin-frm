#### My SQL vs Maria DB

The core underlying executors used thus far (*jasync and vertx*) are named as `MySQL`. But they denote compatibility
with `MaraiDB`. On that note all integration tests are performed with `MariaDB`.
