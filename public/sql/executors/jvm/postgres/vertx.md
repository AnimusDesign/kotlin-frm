The vertx executor is based on the [reactive postgres client](https://vertx.io/docs/vertx-pg-client/kotlin/)

#### Typical Flow

[Look at the upstream documentation on non blocking flow.](./sql/executors/flow.md)

#### Configuration

```kotlin
data class VertxPostgresConfig(
        override val databaseHost: String,
        override val database: String,
        override val databaseUser: String,
        override val databasePassword: String,
        override val databasePort: Int,
        val schema: String,
        val poolSize: Int = 25
)
```

#### Connecting

To instaniate the executor you will need to have a vertx instance.

```kotlin
val vertx = Vertx.vertx()
val executor = VertxPostgresExecutor(vertx)
val success = executor.connect(FRMTestConfig) // Will throw on failure, true on success
```
#### Response

The functions return a response of `AsyncDBResponse` which is a sealed class. Containing the 
following possible responses.

* **SingleResponse** A single response cast to the record. Best for a `select` with only conditions, i.e. `select *`.
* **MultiResponse** Same as before, except returns a `List` of records.
* **MultiMapResponse** Returns a `List<Map<String,Any?>>` Used when performing advanced queries and not passing
    a data class to map into.
* **MultiJoinResponse** Return a `List<AJoinRecord>`, this casts into a custom defined data class. 
* **SaveResponse** Returns a boolean on `update`, `delete`, or `insert.`

##### Crafting a Join Record

To build a custom target join record you provide a data class. Delegated by map. Which
subclasses `AJoinRecord`. This will then call the constructor. Zipping the resulting
row set into a map. With the columns defined on the field.

*Note: If a column is `first_name` we rename to `firstName`*

```kotlin
data class UserProfileJoinRecord(val map: Map<String, Any?>) : AJoinRecord(map) {
    val id: Int by map
    val firstName: String by map
    val lastName: String by map
    val email: String by map
}
```

It would be called with `mapInto` like:

```kotlin
.mapInto(executor, UserProfileJoinRecord::class) 
```
