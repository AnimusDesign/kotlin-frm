The SQL module provides support for traditional relational databases.

* Postgres SQL (*Alpha*)
* MySql / MaraiDB (*In development*)
* SQLite (*Coming Soon*)

The query language / toolset is done in an orthogonal way. A number of
other tools. Take the connection pool, or connection first. Then runs
the query. Instead we take the asyncExecutor last. Decoupling the asyncExecutor from the 
query, allows us to run on multiple platforms, and with multiple executors.

#### Hierarchy

**Schema**

The base module, this will reflect the schema via a gradle plugin.
[See documentation](../sql/schema/intro.md). It will generate
types based off of the given data base. With support for multiple
platforms.

As an example each table will be built as an object. The return
record will be constructed by a record class.

**Query**

The query is a series of function that builds out a data class
representing the query. The query is then built into a type safe
string. Once built it is passed to an asyncExecutor.

To separate matters more. There are two types of queries. A DSL query,
and an FRM query. Both are discussed [here](./query/intro.md).

**Executor**

The asyncExecutor is the most critical part. It takes the query, then submits
that to the asyncExecutor. By having this as the last step it allows us
to work with a number of different futures, and transaction models.
The executors adhere to a common interface, but will return a record
or map based on the code generated via the schema.
      
