During reflection the following table determines the types.

| MariaDB type          	| Java type     	| Javascript 	| Native  	| Common  	|
|-----------------------	|---------------	|------------	|---------	|---------	|
| boolean               	| Boolean       	| Boolean    	| Boolean 	| Boolean 	|
| smallint              	| Short         	| Short      	| Short   	|  Short  	|
| integer (or serial)   	| Int           	| Int        	| Int     	| Int     	|
| bigint (or bigserial) 	| Long          	| Long       	| Long    	| Long    	|
| numeric               	| BigDecimal    	| Number     	| Number  	| Number  	|
| decimal               	| BigDecimal    	| String     	| String  	| String  	|
| new_decimal           	| BigDecimal    	| String     	| String  	| String  	|
| double                	| Double        	| Double     	| Double  	| Double  	|
| text                  	| String        	| String     	| String  	| String  	|
| varchar               	| String        	| String     	| String  	| String  	|
| bpchar                	| String        	| String     	| String  	| String  	|
| timestamp             	| LocalDateTime 	| Long       	| Long    	| Long    	|
| date                  	| LocalDate     	| String     	| String  	| String  	|
| year                  	| Short         	| Short      	| Short   	| Short   	|
| datetime              	| LocalDateTime 	| String     	| String  	| String  	|
| time                  	| Duration      	| String     	| String  	| String  	|
| uuid                  	| UUID          	| String     	| String  	| String  	|
| JSON                  	| String        	| String     	| String  	| String  	|