The SQL DSL can be created in two ways.

**Table Extension Method**

If you are querying one table without doing any joins. This is the easiest
method.

```kotlin
Users.query {
    select() from Users 
}
```

**SQL Query Function**

```kotlin
sqlQuery<Example, UsersRecord, Users> { select() from Users }
```

This takes in several type parameters, these cannot be inferred as it
does not take in a table. A `LockedQuery` is returned from this builder.
The most prominent type is the database.

`<D: IDatabase, R:ADatabaseRecord, T:ITable<D,R>`

* The data base to query, use to ensure scope and only querying items in the same database.
* The record used to cast items into. Largely ignored in more complex queries.
* The table to query. 
