#### Prepared Queries

Prepared queries are supported on both the `FRM` and `DSL` format.
While reflecting the schema `lazy fields` are generated. These are
used as conditional place holders, then during execution a `vararg`
list is passed to the executor.

**Samples**

```kotlin
val query = sqlPreparedQuery<ExampleIDatabase, UsersRecord, Users> {
    update(Users) set {
        Users.firstName TO Users.lazyFirstName
        Users.lastName TO Users.lazyLastName
    } where {
        Users.id equal Users.lazyId
    }
}
```

###### **Note**

**The order of the `vararg` arguments is important. Even if we structure the FRM
queries out of order. It is unbundled in the SQL syntax. That is say if you put
`filter` first in the FRM. It will appear after `column`, `join`, etc. Followed by `groupby`.


#### Querying

The querying is very similar to what follows below for regular `FRM` and `DSL`. 

###### DSL

The dsl for a prepared query is started in two ways. 

```kotlin
        val query = sqlPreparedQuery<ExampleIDatabase, UsersRecord, Users> {
            update(Users) set {
                Users.firstName TO Users.lazyFirstName
                Users.lastName TO Users.lazyLastName
            } where {
                Users.id equal Users.lazyId
            }
        }
```

Or as a nested extension function.

```kotlin
Users.preparedQuery {
    ...
}
```

###### FRM

```kotlin
lazyPreparedSelectJoin(Users)
    .withColumns(Users.id, Users.firstName, Users.lastName, UserProfile.bio)
    .join(UserProfile) {
        UserProfile.id equal Users.lazyId
    }
    .filter {
        Users.firstName iLike Users.lazyLastName
    }
```

As noted from the above examples. They are nearly identical to regular queries.
You just prefix the query to be of a `prepared` type. After that you simply
use a `lazy` field in the query. 

#### Arguments 


```kotlin
lazyPreparedSelectJoin(Users)
    .withColumns(Users.id, Users.firstName, Users.lastName, UserProfile.bio)
    .join(UserProfile) {
        UserProfile.id equal Users.lazyId
    }
    .filter {
        Users.firstName iLike Users.lazyLastName
    }
```

With this example we get the following sql:

```sql
select users.id, users.first_name, users.last_name, user_profile.bio
    from users
    join user_profile on user_profile.id = ?
    where users.first_name ilike ?
```

We see two `?` in that query. Items that will be passed through as varargs. 

```kotlin
map(executor,
    1,     // Will replace the ? in user_profile.id 
    "%ja%" // Will replace the ? in users.first_name
)
```

As you look at the variable arguments think about the query. It doesn't matter
if you put `filter` before `join`. That doesn't change the interpreted values
of the variable arguments. *In short plan out the SQL in your head for the proper order*.

#### Under the Hood

We perform `runtime` error checking instead of compile time checking. Several
things are validated at runtime.

* The number of arguments passed via `varargs` match the number of `lazy fields`
* The type of the `argument` to the related `lazyfield` match.
    * Due to type erasure and multi-platform. We compare on a string of the fully qualified name of the type.
* A constant string is used to place hold the prepared variable statement.
    * Each executor then replaces that string with the executors required prepared statement placeholder.
    