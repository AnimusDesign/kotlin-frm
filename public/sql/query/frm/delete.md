Deleting is relatively straight forward. The table has an extension `deleteOn`.
Which provides the  [`ConditionBuilder DSL`](./sql/query/sql_dsl/condition_builder.md).
To launch the query use `remove` passing in the executor. To wait on completion
utilize `fetch`.

#### Full Query

```kotlin
lazyDelete(Users).deleteOn {
    (Users.firstName equal "Delete") and (Users.lastName equal "Me")
}.remove(executor).fetch()
```