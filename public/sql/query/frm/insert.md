
#### Specifying What to Add

The `add` functions takes in a nested list.  Using a pair of column to the anticipated values.
If the columns do not match up an exception is thrown(`UncommonInsertColumns`).

#### Full Query

```kotlin
val rsp = lazyInsert(Users)
        .add(listOf( // Add multiple records
                listOf(Users.firstName to "Jonnny", Users.lastName to "Johnson"),
                listOf(Users.firstName to "Jimmy", Users.lastName to "Nashorn")
        ))
        .save(executor) 
        .fetch() // Runs an awaits the query
```