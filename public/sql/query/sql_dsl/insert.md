```kotlin
insert(Users) columns listOf(Users.firstName, Users.lastName) values listOf(
        listOf("John", "Doe"),
        listOf("Jane", "Doe")
)
```

```sql
insert into users (users.first_name,users.last_name) values ('John','Doe'),('Jane','Doe')
```

#### Specifying the Table

The insert function takes in either a `table` or `named table`.

```kotlin
insert(Users)
```

```sql
insert into users as people
```

#### Specifying Columns

Columns are specified via an `infix` function that takes in a list of `Fields`.

```kotlin
columns listOf(Users.firstName, Users.lastName)
```

#### Specifying Values

*Warning: the values are not type safe. Meaning they do not validate matching boundary to the field.*

Values are passed as a list of lists. 

```kotlin
values listOf(
    listOf("John", "Doe"),
    listOf("Jane", "Doe")
)
```
